-- BKTINC-116
ALTER TABLE `user_master`
ADD COLUMN `country` varchar(255) NULL AFTER `modify_date`,
ADD COLUMN `state` varchar(255) NULL AFTER `country`,
ADD COLUMN `city` varchar(255) NULL AFTER `state`;

update user_master set country = "Malaysia" WHERE country = "" OR country IS NULL;
update user_master set state = "Wilayah Persekutuan Kuala Lumpur" WHERE state = "" OR state IS NULL;

-- HEREMAP settings --
ALTER TABLE `setting_master`
  ADD COLUMN `android_heremap_app_id` varchar(50) NULL AFTER `createdate`,
  ADD COLUMN `android_heremap_app_code` varchar(50) NULL AFTER `android_heremap_app_id`,
  ADD COLUMN `ios_heremap_app_id` varchar(50) NULL AFTER `android_heremap_app_code`,
  ADD COLUMN `ios_heremap_app_code` varchar(50) NULL AFTER `ios_heremap_app_id`;

-- ADD location columns for order
ALTER TABLE `order_master`
  ADD COLUMN `pickup_city` varchar(255) NULL AFTER `modifieddate`,
  ADD COLUMN `pickup_state` varchar(255) NULL AFTER `pickup_city`,
  ADD COLUMN `pickup_country` varchar(255) NULL AFTER `pickup_state`,
  ADD COLUMN `pickup_postal` varchar(10) NULL AFTER `pickup_country`,
  ADD COLUMN `drop_city` varchar(255) NULL AFTER `pickup_postal`,
  ADD COLUMN `drop_state` varchar(255) NULL AFTER `drop_city`,
  ADD COLUMN `drop_country` varchar(255) NULL AFTER `drop_state`,
  ADD COLUMN `drop_postal` varchar(10) NULL AFTER `drop_country`;

-- Collation
ALTER TABLE `feedback`
MODIFY COLUMN `subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `user_id`,
MODIFY COLUMN `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `subject`;

-- ADD column for integration api
ALTER TABLE `order_master`
  ADD COLUMN `order_items` text NULL AFTER `drop_postal`,
  ADD COLUMN `extra` text NULL AFTER `order_items`,
  ADD COLUMN `merchant_order_id` varchar(100) NULL AFTER `extra`,
  ADD COLUMN `merchant_contact` varchar(20) NULL AFTER `merchant_order_id`,
  ADD COLUMN `order_total` varbinary(20) NULL AFTER `merchant_contact`,
  ADD COLUMN `customer_contact` varchar(20) NULL AFTER `order_total`,
  ADD COLUMN `customer_contact_2` varchar(20) NULL AFTER `customer_contact`;

ALTER TABLE `order_master`
ADD COLUMN `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `rider_accept_time`,
ADD COLUMN `created_by` int(11) DEFAULT NULL AFTER `created_at`,
ADD COLUMN `updated_at` timestamp NULL DEFAULT NULL AFTER `created_by`,
ADD COLUMN `updated_by` int(11) DEFAULT NULL AFTER `updated_at`,
ADD COLUMN `deleted_at` timestamp NULL DEFAULT NULL AFTER `updated_by`;

ALTER TABLE `feedback`
ADD COLUMN `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP AFTER `create_date`,
ADD COLUMN `created_by` int(11) DEFAULT NULL AFTER `created_at`,
ADD COLUMN `updated_at` timestamp NULL DEFAULT NULL AFTER `created_by`,
ADD COLUMN `updated_by` int(11) DEFAULT NULL AFTER `updated_at`,
ADD COLUMN `deleted_at` timestamp NULL DEFAULT NULL AFTER `updated_by`;

ALTER TABLE `merchants`
ADD COLUMN `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP AFTER `user_id`,
ADD COLUMN `created_by` int(11) DEFAULT NULL AFTER `created_at`,
ADD COLUMN `updated_at` timestamp NULL DEFAULT NULL AFTER `created_by`,
ADD COLUMN `updated_by` int(11) DEFAULT NULL AFTER `updated_at`,
ADD COLUMN `deleted_at` timestamp NULL DEFAULT NULL AFTER `updated_by`;

ALTER TABLE `user_master`
ADD COLUMN `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP AFTER `city`,
ADD COLUMN `created_by` int(11) DEFAULT NULL AFTER `created_at`,
ADD COLUMN `updated_at` timestamp NULL DEFAULT NULL AFTER `created_by`,
ADD COLUMN `updated_by` int(11) DEFAULT NULL AFTER `updated_at`,
ADD COLUMN `deleted_at` timestamp NULL DEFAULT NULL AFTER `updated_by`;

CREATE TABLE `wallet_transaction`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(0) UNSIGNED NULL,
  `order_id` int(0) NULL,
  `amount` varchar(20) NULL,
  `tip` varchar(20) NULL,
  `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(0) UNSIGNED NULL,
  `updated_at` timestamp(0) NULL,
  `updated_by` int(0) NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `order_master`
MODIFY COLUMN `user_cash_payment` decimal(8, 0) NOT NULL AFTER `ordertype`;

ALTER TABLE `order_master`
  ADD COLUMN `cancel_reason` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `order_total`;

ALTER TABLE `order_master`
  ADD COLUMN `cancel_time` datetime(0) NULL AFTER `customer_contact_2`;

ALTER TABLE `order_master`
  ADD COLUMN `rider_accept_time` datetime(0) NULL AFTER `cancel_time`;

ALTER TABLE `order_master`
ADD COLUMN `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `rider_accept_time`,
ADD COLUMN `created_by` int(11) DEFAULT NULL AFTER `created_at`,
ADD COLUMN `updated_at` timestamp NULL DEFAULT NULL AFTER `created_by`,
ADD COLUMN `updated_by` int(11) DEFAULT NULL AFTER `updated_at`,
ADD COLUMN `deleted_at` timestamp NULL DEFAULT NULL AFTER `updated_by`;

ALTER TABLE `feedback`
ADD COLUMN `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP AFTER `create_date`,
ADD COLUMN `created_by` int(11) DEFAULT NULL AFTER `created_at`,
ADD COLUMN `updated_at` timestamp NULL DEFAULT NULL AFTER `created_by`,
ADD COLUMN `updated_by` int(11) DEFAULT NULL AFTER `updated_at`,
ADD COLUMN `deleted_at` timestamp NULL DEFAULT NULL AFTER `updated_by`;

ALTER TABLE `merchants`
ADD COLUMN `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP AFTER `user_id`,
ADD COLUMN `created_by` int(11) DEFAULT NULL AFTER `created_at`,
ADD COLUMN `updated_at` timestamp NULL DEFAULT NULL AFTER `created_by`,
ADD COLUMN `updated_by` int(11) DEFAULT NULL AFTER `updated_at`,
ADD COLUMN `deleted_at` timestamp NULL DEFAULT NULL AFTER `updated_by`;

ALTER TABLE `user_master`
ADD COLUMN `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP AFTER `city`,
ADD COLUMN `created_by` int(11) DEFAULT NULL AFTER `created_at`,
ADD COLUMN `updated_at` timestamp NULL DEFAULT NULL AFTER `created_by`,
ADD COLUMN `updated_by` int(11) DEFAULT NULL AFTER `updated_at`,
ADD COLUMN `deleted_at` timestamp NULL DEFAULT NULL AFTER `updated_by`;

CREATE TABLE `wallet_transaction`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(0) UNSIGNED NULL,
  `order_id` int(0) NULL,
  `amount` varchar(20) NULL,
  `tip` varchar(20) NULL,
  `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(0) UNSIGNED NULL,
  `updated_at` timestamp(0) NULL,
  `updated_by` int(0) NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `order_master`
MODIFY COLUMN `user_cash_payment` decimal(8, 0) NOT NULL AFTER `ordertype`;

ALTER TABLE `order_master`
  ADD COLUMN `cancel_reason` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `order_total`;

ALTER TABLE `order_master`
  ADD COLUMN `cancel_time` datetime(0) NULL AFTER `customer_contact_2`;

ALTER TABLE `order_master`
  ADD COLUMN `rider_accept_time` datetime(0) NULL AFTER `cancel_time`;

ALTER TABLE `order_master`
MODIFY COLUMN `distance` varchar(5) NOT NULL AFTER `tips`;

ALTER TABLE `order_master`
ADD COLUMN `current_round` tinyint(2) NULL DEFAULT 0 AFTER `distance`;

ALTER TABLE `order_master`
CHANGE COLUMN `orderid` `id` int(20) NOT NULL AUTO_INCREMENT FIRST;

ALTER TABLE `user_master`
CHANGE COLUMN `userid` `id` int(20) NOT NULL AUTO_INCREMENT FIRST;

ALTER TABLE `message`
ADD COLUMN `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP AFTER `create_date`,
ADD COLUMN `created_by` int(11) DEFAULT NULL AFTER `created_at`,
ADD COLUMN `updated_at` timestamp NULL DEFAULT NULL AFTER `created_by`,
ADD COLUMN `updated_by` int(11) DEFAULT NULL AFTER `updated_at`,
ADD COLUMN `deleted_at` timestamp NULL DEFAULT NULL AFTER `updated_by`;

ALTER TABLE `order_master`
ADD COLUMN `merchant_id` int(0) UNSIGNED NULL AFTER `id`;

update order_master o set o.merchant_id = (SELECT id FROM merchants m where m.user_id = o.userid)

-- User Session
ALTER TABLE `user_session`
MODIFY COLUMN `token` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL AFTER `userid`;

--
ALTER TABLE `user_master`
DROP COLUMN `fbid`,
DROP COLUMN `referralid`,
DROP COLUMN `wallet`,
DROP COLUMN `carry_forward`,
DROP COLUMN `clat`,
DROP COLUMN `clng`,
DROP COLUMN `front_photo`,
DROP COLUMN `back_photo`,
DROP COLUMN `ic_front_photo`,
DROP COLUMN `ic_back_photo`,
DROP COLUMN `bike_number_plate`,
DROP COLUMN `bike_pic`,
DROP COLUMN `insurance_latter_pic`,
DROP COLUMN `road_tex_pic`,
DROP COLUMN `subscription_plan`,
DROP COLUMN `sub_enddate`,
ADD COLUMN `max_load` smallint(0) UNSIGNED NULL AFTER `commission`,
ADD COLUMN `current_load` smallint(0) UNSIGNED NULL AFTER `max_load`;

-- Change datetime to timestamp data type
ALTER TABLE `order_master`
MODIFY COLUMN `pickup_time` timestamp(0) NULL DEFAULT NULL AFTER `no_of_km`,
MODIFY COLUMN `arrive_pickup_time` timestamp(0) NOT NULL AFTER `pickup_time`,
MODIFY COLUMN `reached_destination_time` timestamp(0) NULL DEFAULT NULL AFTER `is_reached_destination`,
MODIFY COLUMN `at_destination` timestamp(0) NOT NULL AFTER `reached_destination_time`,
MODIFY COLUMN `dropoff_time` timestamp(0) NOT NULL AFTER `is_at_destination`,
MODIFY COLUMN `at_dropoff_time` timestamp(0) NOT NULL AFTER `is_at_dropoff`,
MODIFY COLUMN `orderdate` timestamp(0) NOT NULL AFTER `signature_image`,
MODIFY COLUMN `delivery_time` timestamp(0) NOT NULL AFTER `rider_deleted`,
MODIFY COLUMN `cancel_time` timestamp(0) NULL DEFAULT NULL AFTER `customer_contact_2`,
MODIFY COLUMN `rider_accept_time` timestamp(0) NULL DEFAULT NULL AFTER `cancel_time`;

-- Change pricing data type
ALTER TABLE `holiday_pricing`
MODIFY COLUMN `min_km` varchar(10) NOT NULL AFTER `holiday_date`,
MODIFY COLUMN `min_km_re` varchar(10) NOT NULL AFTER `min_km`,
MODIFY COLUMN `min_km_price_re` varchar(10) NOT NULL AFTER `min_km_re`,
MODIFY COLUMN `min_km_price_csd_fd` varchar(10) NOT NULL AFTER `min_km_price_re`,
MODIFY COLUMN `min_up_km` varchar(10) NOT NULL AFTER `min_km_price_csd_fd`,
MODIFY COLUMN `min_up_km_re` varchar(10) NOT NULL AFTER `min_up_km`,
MODIFY COLUMN `min_up_price` varchar(10) NOT NULL AFTER `min_up_km_re`,
MODIFY COLUMN `min_up_price_re` varchar(10) NOT NULL AFTER `min_up_price`;

-- Add column for verify order
ALTER TABLE `order_master`
ADD COLUMN `need_image_verify` tinyint(0) NULL DEFAULT 1 AFTER `receipt`,
ADD COLUMN `image_verify_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'signature' AFTER `need_image_verify`;

-- Add columns for order payment
ALTER TABLE `order_master`
ADD COLUMN `transaction_id` int(0) UNSIGNED NULL AFTER `id`,
ADD COLUMN `shipping_amount` varchar(255) NULL AFTER `ordertype`,
ADD COLUMN `pickup_amount` varchar(255) NULL AFTER `shipping_amount`,
ADD COLUMN `dropoff_amount` varchar(255) NULL AFTER `pickup_amount`;

update order_master set shipping_amount = user_cash_payment, pickup_amount = order_total, dropoff_amount = order_total;

-- Add columns for return
ALTER TABLE `order_master`
ADD COLUMN `is_return` tinyint(0) NULL DEFAULT 0 AFTER `at_dropoff_time`;

-- Add columns for note
ALTER TABLE `notification_master`
ADD COLUMN `note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `createdate`;

-- Add column for status receive order
ALTER TABLE `user_master`
ADD COLUMN `can_receive_order` tinyint(1) NULL DEFAULT 0 AFTER `deleted_at`;

UPDATE user_master set can_receive_order = 1 where createdate < '2019-10-11 00:00:00'

-- Add column for last location update
ALTER TABLE `user_master`
ADD COLUMN `last_location_updated` timestamp(0) NULL AFTER `lng`;

UPDATE user_master SET last_location_updated =  modify_date WHERE last_location_updated IS NULL

-- Add column for rider start order time
ALTER TABLE `order_master`
ADD COLUMN `start_time` timestamp(0) NULL AFTER `no_of_km`;