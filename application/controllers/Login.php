<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {



    function __construct() {

        parent::__construct();                

    }

    function index() {

        redirect('admin/login');

    }

    function verify($verifycode ='') {
    	$this->load->model('user_model');
    	$data ="";
    	if(!empty($verifycode)){
    		$user_data = array("emailverify"=>'');
    		$data = $this->user_model->getUserbyToken($verifycode);
    		if(!empty($data)){
				if($data->usertype == "user"){
					$user_data['is_verify'] =1;
				}
    			$this->user_model->updateUser($user_data, $data->userid);	
    			echo "User verified successfully please Login..";
    		} else {
    			echo "You are already active please Login..";
    		}
    	} else {
    		echo "Token Expired...";
    	}
    }
}

