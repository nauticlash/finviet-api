<?php

class Rider extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function login()
    {
        $this->load->model('user_model');

        $data = json_decode($this->input->raw_input_stream);
        $request = $data->request;
        $email = $request->email;
        $password = $request->password;
        $usertype = $request->usertype;
        $deviceid = $request->deviceid;
        $platform = $request->platform;

        $login = $this->user_model->userLogin($email, $password, $usertype);

        if ($login) {
            if ($login->status) {
                $this->r_data['message'] = 'Tài khoản của bạn đã bị khóa.';
                return $this->returnData();
            }
            $login->newnotification = $this->get_unread_counter($login->id, $usertype);
            if ($login->is_verify == '0') {
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'Chưa xác thực OTP.';
                $this->r_data['is_verify'] = $login->is_verify;
                $this->r_data['data'] = $login;
            } else if ($login->is_verify == '1') {
                $this->r_data['token'] = $this->common->getToken($login);
                $user_session = array(
                    'userid' => $login->id,
                    'token' => $this->r_data['token'],
                    'login_type' => 'login',
                    'start_date' => DATETIME,
                );
                if (!empty($login->deviceid) && $login->deviceid != $deviceid) {
                    $this->load->model('common_model');
                    $message = 'Tài khoản của bạn đã được đăng nhập ở thiết bị khác.';
                    $data = array("message" => $message, "data" => array("userid" => $login->userid), "mtype" => "admin_disable");
                    $msg = array ( 'title' => 'Thông báo', 'body' => $message );
                    $this->common_model->sendPushNotification([$login->deviceid], $data, $msg, $login->platform);
                }
                if (!empty($deviceid)) {
                    $this->user_model->updateDeviceUser(array('platform' => "", 'deviceid' => ""), $deviceid);
                    $this->user_model->updateUser(array('platform' => $platform, 'online_status' => 'online', 'deviceid' => $deviceid), $login->id);
                }
                $this->r_data['secret_log_id'] = $this->common->insertUserSession($user_session);
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'Login Successful.';

                $this->r_data['data'] = new stdClass();
                $this->r_data['data'] = $login;
                $this->r_data['data']->totalunread_msgs = $this->user_model->getUnreadMessagecounts($login->id);
            }
        } else {
            $this->r_data['message'] = 'Số điện thoại hoặc mật khẩu không đúng.';
        }
        return $this->returnData();
    }

    /**
     * Logout
     */
    public function logout($secret_log_id)
    {
        $this->load->model('user_model');
        $session = $this->common->getSessionInfo($secret_log_id);
        if ($session) {
            if ($session->userid != $this->userid) {
                $this->r_data['message'] = 'Secret log does not belongs to you.';
                return $this->returnData();
            }
            $this->user_model->updateUser(array('online_status' => 'offline'), $this->userid);
            $this->common->logoutUser($secret_log_id);
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Logout Successful.';
        }
        return $this->returnData();
    }

    /**
     * create referral code
     */

    public function referral_code($limit)
    {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }

    /**
     *  Check Email Contact
     */

    public function CheckEmailContact($data)
    {

        $user_data = array(
            'usertype' => isset($data->usertype) ? $data->usertype : '',
            'email' => isset($data->email) ? $data->email : '',
            'contactno' => isset($data->contactno) ? $data->contactno : '',
        );

        $userdata = array('usertype');
        $validation = $this->paramValidation($userdata, $user_data);
        if ($validation['success'] == 0) {
            return $this->returnData();
        }

        $check = false;
        $mcheck = false;
        $this->load->model('user_model');

        if ($data->email) {
            $check = $this->user_model->checkUserExists('email', $data->email, $data->usertype);
        }
        if ($check) {
            $this->r_data['success'] = 0;
            $this->r_data['message'] = 'Email already registered!';
            return $this->returnData();
        }

        if ($data->contactno) {
            $mcheck = $this->user_model->checkUserExists('contactno', $data->contactno, $data->usertype);
        }

        if ($mcheck) {
            $this->r_data['success'] = 0;
            $this->r_data['message'] = 'Mobile number already registered!';
            return $this->returnData();
        }

        $this->r_data['success'] = 1;
        $this->r_data['message'] = 'Email mobile verify success';
        return $this->returnData();
    }

    /**
     * Rider Register
     */
    public function rider_register($data)
    {

        $this->load->model('common_model');
        $user_data = array(
            'usertype' => isset($data->usertype) ? $data->usertype : '',
            'firstname' => isset($data->firstname) ? $data->firstname : '',
            'email' => isset($data->email) ? $data->email : '',
            'password' => isset($data->password) ? md5($data->password) : '',
            'platform' => isset($data->platform) ? $data->platform : '',
            'deviceid' => isset($data->deviceid) ? $data->deviceid : '',
            'contactno' => isset($data->contactno) ? $data->contactno : '',
            'address' => isset($data->address) ? $data->address : '',
            'dob' => isset($data->dob) ? $data->dob : '',
            'lat' => isset($data->lat) ? $data->lat : '',
            'lng' => isset($data->lng) ? $data->lng : '',
            'is_verify' => '0',
            'online_status' => 'offline',
            'country' => isset($data->country) ? $data->country : '',
            'state' => isset($data->state) ? $data->state : '',
            'city' => isset($data->city) ? $data->city : '',
        );

        if (!empty($data->profilepic)) {
            $user_data['profilepic'] = $data->profilepic;
        }

        $user_data['contactno'] = str_replace(["(", ")", "-", " "], "", $user_data['contactno']);
        $user_data['contactno'] = preg_replace("/^\+?(84)/", "0", $user_data['contactno']);

        $userdata = array('usertype', 'firstname');
        $validation = $this->paramValidation($userdata, $user_data);
        if ($validation['success'] == 0) {
            return $this->returnData();
        }

        $this->load->model('user_model');

        $chars = "0123456789";
        $code = $user_data['code'] = substr(str_shuffle($chars), 0, 4);
        $existsUser = $this->user_model->getExistsUser($user_data['contactno'], $data->usertype);
        if (!empty($existsUser)) {
            if ($existsUser->is_verify) {
                $this->r_data['success'] = 0;
                $this->r_data['message'] = 'Số điện thoại đã đăng ký với hệ thống';
                return $this->returnData();
            } else {
                $this->user_model->deleteUser($existsUser->id);
            }
        }

        $verifycode = $this->common->getSecureKey();

        $user_data['emailverify'] = $verifycode;
        $userid = $this->user_model->addUser($user_data);
        //Add user session
        $this->r_data['token'] = $this->common->getSecureKey();
        $user_session = array(
            'userid' => $userid,
            'token' => $this->r_data['token'],
            'login_type' => 'verify',
            'start_date' => DATETIME,
        );
        $this->common->insertUserSession($user_session);

        $this->r_data['success'] = 1;
        $this->r_data['code'] = $code;
        $this->r_data['userid'] = $userid;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, otp_api);

        $data = [
            'username' => otp_username,
            'password' => otp_pass,
            'phones' => [$data->contactno],
            'smsContent' => 'Ma OTP cua ban la ' . $code,
            'reqid' => otp_reqid
        ];
        $headers = array(
            'Content-Type:application/json'
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        curl_exec($ch);
        curl_close($ch);
        return $this->returnData();
    }

    /**
     * Verify User
     */
    public function verify_user($code)
    {
        $this->load->model('user_model');
        $user_details = $this->user_model->getUserWithSession($this->token_id, 'verify');
        $verify = false;
        if ($user_details) {
            if ($code && $user_details->code == $code) {
                $verify = true;
                $this->user_model->updateUser(array('is_verify' => 1, 'code' => 0), $this->userid);
                $this->common->logoutUser($user_details->session_id);
            }
        }

        if ($verify) {
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Successfully verified.';
            $this->r_data['data'] = $this->user_model->getUser($this->userid);
        } else {
            $this->r_data['message'] = 'User not verified.';
        }
        return $this->returnData();
    }

    /**
     * Resend Code
     */
    public function resend_code($contactno)
    {
        $this->load->model('user_model');
        $user_details = $this->user_model->checkUserExists('contactno', $contactno, 'driver');
        $this->r_data['message'] = 'User already verified.';
        if (!$user_details) {
            $this->r_data['message'] = 'User not found.';
        }

        $user_data = $this->user_model->getUserWithSession($this->token_id, 'verify');
        if ($user_data && $user_details && $user_details->is_verify == 0) {
            $chars = "0123456789";
            $code = substr(str_shuffle($chars), 0, 4);
            $this->user_model->updateUser(array('code' => $code), $this->userid);
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, otp_api);

            $data = [
                'username' => otp_username,
                'password' => otp_pass,
                'phones' => [$contactno],
                'smsContent' => 'Ma OTP cua ban la ' . $code,
                'reqid' => otp_reqid
            ];
            $headers = array(
                'Content-Type:application/json'
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

            curl_exec($ch);
            curl_close($ch);
            $this->r_data['success'] = 1;
            $this->r_data['code'] = $code;
            $this->r_data['message'] = 'Verification code successfully sent.';
        }
        return $this->returnData();
    }

    /**
     * Update Location
     */
    public function updateLocation($userid, $clat, $clng)
    {
        $this->load->model('user_model');
        $user_details = $this->user_model->getUser($userid);
        if (!$user_details) {
            $this->r_data['message'] = 'User not found.';
        }

        if ($user_details) {
            $clat = str_replace(":", "", $clat);

            $this->user_model->updateLocation($userid, $clat, $clng);
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Location Update successfully';
        }
        return $this->returnData();
    }
    /*
         * get counter for unread notification
         */

    public function get_unread_counter($userid, $usertype)
    {
        $this->load->model('user_model');
        $this->load->model('dbcommon');
        $user_data = $this->user_model->getUser($userid);
        if ($user_data) {
            if ($usertype == 'driver') {
                $where = "where `status` IN('assign_by_user','reject_by_user','complete_by_user','neworder','make_payment','change_accepted','give_tips','payment_at_des','cancelorder','admin') AND riderid = '" . $userid . "' AND isview = '0'";
            }

            if ($usertype == 'user') {
                $where = "where `status` IN('pickup','dropoff','at_destination','bid','complete_by_rider','accept_payment','change_paid','reached_destination','at_dropoff','payment_at_des','cancelorder','arrivepickup','admin','nobid') AND userid = '" . $userid . "' AND isview = '0'";
            }

            $counter = $this->dbcommon->getnumofdetails($this->common->getNotificationTable(), $where);
            if ($counter > 0) {
                return $counter;
            } else {
                return "0";
            }
        } else {
            return "0";
        }
    }
}
