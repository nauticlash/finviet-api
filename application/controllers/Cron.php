<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cron extends CI_Controller
{

    public function __construct()
    {
		if(!is_cli()) {
			exit("No direct script access allowed");
		}

        parent::__construct();

    }

    public function index($verifycode = '')
    {
        $to_time = date('Y-m-d H:i:s');
        $start_time = date('Y-m-d H:i:s', strtotime("-3 minutes", strtotime($to_time)));
        $this->load->model('user_model');
        $this->load->model('dbcommon');
        $this->load->model('common_model');
        $query = "SELECT o.orderid,u.userid,u.firstname,u.lastname,u.platform,u.deviceid,(SELECT COUNT(order_id) FROM notification_master WHERE order_id = o.orderid AND `status` = 'bid') AS isbided FROM order_master as o JOIN user_master u ON u.userid = o.userid WHERE o.`status` = 'Pending' AND o.admincancel='' AND o.orderdate <= '" . $start_time . "'  HAVING isbided = 0";

        $userdata = $this->dbcommon->getInfo_($query, 1);
        foreach ($userdata as $userresult) {
            $noti_data = array(
                "userid" => $userresult->userid,
                "riderid" => $userresult->userid,
                //"message" => 'Your Order #' . $userresult->orderid .' has been Canceled please reorder',
                "message" => 'Sorry, All our Roadies are busy, please Re-order. You can also add a Tip to draw more Roadies to take up your task. Thank you.',
                "order_id" => $userresult->orderid,
                "type" => "nobid",
                "status" => "nobid",
                "createdate" => date('Y-m-d H:i:s'),
            );
            //$this->user_model->saveNotification($noti_data);
            // 'status' => 'Canceled','riderstatus' => 'Complete',
            $orderdata = array('admincancel' => "Sorry, it looks like all our Roadies are currently busy. Please Re-Order. You can also add a Tip to draw more Roadies to take up your task. Thank you");
            $orderupdate = $this->user_model->updateorderbyid($userresult->orderid, $orderdata);
            if ($orderupdate) {

                $oquery = "SELECT ordertype FROM order_master WHERE orderid = '" . $userresult->orderid . "'";
                $roquery = $this->dbcommon->getInfo_($oquery);
                $noti_data['ordertype'] = $roquery->ordertype;

                //$ntmsg = 'Your Order #' . $userresult->orderid .' has been Canceled please reorder';
                $ntmsg = 'Sorry, it looks like all our Roadies are currently busy. Please Re-Order. You can also add a Tip to draw more Roadies to take up your task. Thank you';
                $message = array("message" => $ntmsg, "data" => $noti_data, "mtype" => "nobid");
                //$messageios = $ntmsg;
                $msg = array('title' => 'Bungkusit', 'body' => $ntmsg);
                $iosdeviceids = $and_ids = array();

                if ($userresult->platform == 'ios' && !empty($userresult->deviceid)) {
                    $iosdeviceids[] = $userresult->deviceid;
                } else if ($userresult->platform == 'android' && !empty($userresult->deviceid)) {
                    $and_ids[] = $userresult->deviceid;
                }
            }
        }
        if (!empty($and_ids)) {
            $this->common_model->sendPushNotification($and_ids, $message, $msg, 'android');
            $this->r_data['notification'] = 1;
        }
        if (!empty($iosdeviceids)) {
            $this->common_model->sendPushNotification($iosdeviceids, $message, $msg, 'ios');
            $this->r_data['notification'] = 1;
        }
    }

    public function persitLocation()
    {
        $this->load->library('redis_client');
		$values = $this->redis_client->getCachedLocationValues();
		if(empty($values)){
			log_message('info', 'empty cached!' );
			return false;
		}
		$data = [];
		$sql = 'insert into user_master (lat,lng,modify_date,userid) values ';
        foreach ($values as $val) {
            $a = explode(',', $val);
            if (count($a) == 4) {
                // $data[] = [
                //     'lat' => $a[0],
                //     'lng' => $a[1],
                //     'modify_date' => $a[2],
                //     'userid' => $a[3],
				// ];
				$data[] = '('. $val . ')';
				
            } else {
				log_message('error', 'cannot parse redis result:' . json_encode($val));
			}
		}
		log_message('info', 'data:'.json_encode($data));

       // $sql = $this->db->insert_string('user_master', $data);
		$sql .= implode(',',$data);
        $sql .= " on duplicate key update lat = values(lat), lng = values(lng), modify_date = values(modify_date) ";
        $this->db->query($sql);
    }
    
	
	 /**
     * for testing purpose
     */
    public function loadLocationFromDB()
    {
        $this->load->model('user_model');
        $user_details = $this->user_model->getOnlineRiders();
        //log_message('debug', 'aaaaaaaaa:'. json_encode($user_details));
        $this->load->library('redis_client');
        $this->redis_client->clearAllLocations();
        foreach ($user_details as $user) {
            $this->redis_client->updateLocation($user->userid, $user->lat, $user->lng);
        }
        $this->r_data['success'] = 1;
        $this->r_data['message'] = 'Status Change Successfully';
        return $this->r_data;
    }

}
