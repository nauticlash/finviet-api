<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	
	public function uploadprofile()
	{
		$proimage_name = "";
		if (!empty($_FILES['profilepic']['name'])) 
		{
			$file = $_FILES['profilepic'];
			
			$FilePath = '../images/profile';
			if (!file_exists($FilePath))
			{
				mkdir($FilePath, 0777, true);
			}
			$filename = stripslashes($file['name']);
			$extension = $this->getExtension($filename);
			$extension = strtolower($extension);
			//$img_name = explode('.'.$extension,$file['name']);
			$image_name= 'profile'.rand().'.'.$extension;
			$FilePath .="/".$image_name;
			if($copied = move_uploaded_file($file["tmp_name"], $FilePath))
			{
				 $proimage_name = $image_name;
				 $response['success'] = "0";
				 $response['profilepic'] = $proimage_name;
			} 
		} else {
			$response['success'] = "1";
			$response['profilepic'] = $proimage_name;
		}
		
		echo json_encode($response);
	}
	
	function getExtension($str)
	{
		$i = strrpos($str,".");
		if (!$i) 
		{
			return "";
		}
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}
}
?>