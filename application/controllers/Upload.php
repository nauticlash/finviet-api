<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->r_data['message'] = $this->r_data['data'] = '';
        $this->r_data['success'] = 0;
    }

    /* user profile pic upload */
    public function uploadprofilepic()
    {
        $this->load->model('user_model');
        $this->r_data['message'] = 'Upload error..!';
        $imagepath =  UPLOAD_IMAGE_URL . '/profile/';

        $is_upload = false;
        if (isset($_FILES['image']['name'])) {
            $temp_file = $_FILES['image']['tmp_name'];
            $img_name = mt_rand(10000, 999999999) . time();
            $path = $_FILES['image']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $image_data['image'] = $img_name . "." . $ext;
            $url = $imagepath. $image_data['image'];
            $this->gdb->compress_image($temp_file, $url, 80);
            $is_upload = true;
        }
        if ($is_upload) {
            $this->r_data['data'] = $image_data['image'];
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Successfully Uploaded..!';
        }
        return $this->return_data($this->r_data);
    }

    public function uploadcourierImage(){
        
        $this->load->model('user_model');
        $this->r_data['message'] = 'Upload error..!';
        if(isset($_FILES["images"]["tmp_name"]) && !empty($_FILES["images"]["tmp_name"]))
        { 
        $imagepath =  UPLOAD_IMAGE_URL . '/courier_images/';
        $output = array();
         foreach($_FILES["images"]["tmp_name"] as $key=>$tmp_name){
             $temp = $_FILES["images"]["tmp_name"][$key];
             $path = $_FILES['images']['name'][$key];
             $ext = pathinfo($path, PATHINFO_EXTENSION);
             $name = mt_rand(10000, 999999999) . time(). "." . $ext;
             $url = $imagepath. $name;    
             if(!empty($temp))
             {
                array_push($output,$name);
                $this->gdb->compress_image($temp,  $url, 80);
             }
         }
        $is_upload = false;
        if(!empty($output))
        {
             $image_data = implode(',', $output);
             $is_upload = true;
        }

        if ($is_upload) {
            $this->r_data['data'] = $image_data;
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Successfully Uploaded..!';
        }
       }//if empty image
        return $this->return_data($this->r_data);
    }

     
  

    public function ImageVideo(){
        
        $this->load->model('user_model');
        $this->r_data['message'] = 'Upload error..!';

        //$token_id = isset($_POST['token_id']) ? trim($_POST['token_id']) : '';
        
        //$user_data = $this->user_model->getUserWithSession($token_id, 'login');
        //if(!$user_data){
        //    $this->r_data['message'] = 'User not found..!';
        //    $this->r_data['token'] = $token_id;
        //    return $this->return_data($this->r_data);
        //}

        $is_upload = false;
        if (isset($_FILES['que_image']['name'])) {
            $temp_file = $_FILES['que_image']['tmp_name'];
            $img_name = mt_rand(10000, 999999999) . time();
            $path = $_FILES['que_image']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $image_data['que_image'] = $img_name . "." . $ext;
            $url = POST . $image_data['que_image'];
            if( $ext == 'png' || $ext == 'jpg' || $ext == 'gif' || $ext == 'jpeg'){
               $this->gdb->compress_image($temp_file, $url, 80);
               $is_upload = true;
            }   
            else if($ext == 'mp4')
            {
               if(move_uploaded_file($temp_file,$url))
		{
			$is_upload = true;
		}
		else
		{ 
		        $is_upload = false;
		}
            }
            else 
            {
             	$is_upload = false;
            }
        }
        if ($is_upload) {
            $this->r_data['data'] = $image_data['que_image'];
            $this->r_data['success'] = 1;
            //$this->r_data['token'] = $token_id;
            $this->r_data['message'] = 'Successfully Uploaded..!';
        }
        return $this->return_data($this->r_data);
    }

    public function return_data($data) {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit();
    }
    
    
    /* user profile pic upload */
    public function uploadmessageattachment()
    {
        $this->load->model('user_model');
        $this->r_data['message'] = 'Upload error..!';
        $imagepath =  UPLOAD_IMAGE_URL . '/message/';

        $is_upload = false;
        if (isset($_FILES['image']['name'])) {
            $temp_file = $_FILES['image']['tmp_name'];
            $img_name = mt_rand(10000, 999999999) . time();
            $path = $_FILES['image']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $image_data['image'] = $img_name . "." . $ext;
            $url = $imagepath. $image_data['image'];
            $this->gdb->compress_image($temp_file, $url, 80);
            $is_upload = true;
        }
        if ($is_upload) {
            $this->r_data['data'] = $image_data['image'];
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Successfully Uploaded..!';
        }
        return $this->return_data($this->r_data);
    }
    /*
     * upload subscription receipt
     */
    public function uploadsubscriptionImage()
    {
        $this->load->model('user_model');
        $this->r_data['message'] = 'Upload error..!';
        $imagepath =  UPLOAD_IMAGE_URL . '/sub_receipt/';
        $is_upload = false;
        if (isset($_FILES['image']['name'])) {
            $temp_file = $_FILES['image']['tmp_name'];
            $img_name = mt_rand(10000, 999999999) . time();
            $path = $_FILES['image']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $image_data['image'] = $img_name . "." . $ext;
            $url = $imagepath. $image_data['image'];
            $this->gdb->compress_image($temp_file, $url, 80);
            $is_upload = true;
        }
        if ($is_upload) {
            $this->r_data['data'] = $image_data['image'];
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Successfully Uploaded..!';
        }
        return $this->return_data($this->r_data);
    }

}

