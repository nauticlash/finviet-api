<?php
/**
 * Description of ServiceMethods
 *
 * @author Dinesh Ninave
 */

class ServiceMethods
{

    /**
     * Services on which authentication not required.
     *
     * @var array
     */
    private $open_service = array('login', 'register', 'update_read_notification', 'rider_register', 'forgotPassword', 'forgotPassword1', 'verifyForgotOTP', 'resend_code', 'get_fareprice', 'CheckEmailContact', 'get_unread_counter', 'get_version', 'get_settings', 'test_api', 'delete_notification', 'admin_cancel_order', 'refind_rider');

    public function __construct()
    {
        $this->r_data['message'] = '';
        $this->r_data['success'] = 0;
        // $this->load->model('common_model');
    }

    public function paramValidation($paramarray, $data)
    {
        $NovalueParam = '';
        foreach ($paramarray as $val) {
            if ($data[$val] == '') {
                $NovalueParam[] = $val;
            }
        }
        if (is_array($NovalueParam) && count($NovalueParam) > 0) {
            $this->r_data['message'] = 'Sorry, that is not valid input. You missed ' . implode(', ', $NovalueParam) . ' parameters';
        } else {
            $this->r_data['success'] = 1;
        }
        return $this->r_data;
    }

    public function login($email, $password, $usertype, $platform, $deviceid, $fbid, $firstname)
    {
        $this->load->model('user_model');

        $login = $this->user_model->userLogin($email, $password, $usertype);

        if ($login) {
            if ($login->status) {
                $this->r_data['message'] = 'Tài khoản của bạn đã bị khóa.';
                return $this->r_data;
            }
            $login->newnotification = $this->get_unread_counter($login->id, $usertype);
            if ($login->is_verify == '0') {
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'Chưa xác thực OTP.';
                $this->r_data['is_verify'] = $login->is_verify;
                $this->r_data['data'] = $login;
            } else if ($login->is_verify == '1') {
                //Add user session
//                $this->r_data['token'] = $this->common->getSecureKey();
                $this->r_data['token'] = $this->common->getToken($login);
                $user_session = array(
                    'userid' => $login->id,
                    'token' => $this->r_data['token'],
                    'login_type' => 'login',
                    'start_date' => DATETIME,
                );
                if (!empty($login->deviceid) && $login->deviceid != $deviceid) {
                    $this->load->model('common_model');
                    $message = 'Tài khoản của bạn đã được đăng nhập ở thiết bị khác.';
                    $data = array("message" => $message, "data" => array("userid" => $login->userid), "mtype" => "admin_disable");
                    $msg = array ( 'title' => 'Thông báo', 'body' => $message );
                    $this->common_model->sendPushNotification([$login->deviceid], $data, $msg, $login->platform);
                }
                if (!empty($deviceid)) {
                    $this->user_model->updateDeviceUser(array('platform' => "", 'deviceid' => ""), $deviceid);
                    $this->user_model->updateUser(array('platform' => $platform, 'online_status' => 'online', 'deviceid' => $deviceid), $login->id);
                }
                $this->r_data['secret_log_id'] = $this->common->insertUserSession($user_session);
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'Login Successful.';

                $this->r_data['data'] = new stdClass();
                $this->r_data['data'] = $login;
                $this->r_data['data']->totalunread_msgs = $this->user_model->getUnreadMessagecounts($login->id);
            }
        } else {
            $this->r_data['message'] = 'Số điện thoại hoặc mật khẩu không đúng.';
        }
        return $this->r_data;
    }

    /**
     * Logout
     */
    public function logout($secret_log_id)
    {
        $this->load->model('user_model');
        $session = $this->common->getSessionInfo($secret_log_id);
        if ($session) {
            if ($session->userid != $this->userid) {
                $this->r_data['message'] = 'Secret log does not belongs to you.';
                return $this->r_data;
            }
            $this->user_model->updateUser(array('online_status' => 'offline'), $this->userid);
            $this->common->logoutUser($secret_log_id);
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Logout Successful.';
        }
        return $this->r_data;
    }
    /*
     * create referral code
     */

    public function referral_code($limit)
    {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }

    /*
     *  Check Email Contact
     */

    public function CheckEmailContact($data)
    {

        $user_data = array(
            'usertype' => isset($data->usertype) ? $data->usertype : '',
            'email' => isset($data->email) ? $data->email : '',
            'contactno' => isset($data->contactno) ? $data->contactno : '',
        );

        $userdata = array('usertype');
        $validation = $this->paramValidation($userdata, $user_data);
        if ($validation['success'] == 0) {
            return $this->r_data;
        }

        $check = false;
        $mcheck = false;
        $this->load->model('user_model');

        if ($data->contactno) {
            $mcheck = $this->user_model->checkUserExists('contactno', $data->contactno, $data->usertype);
        }

        if ($mcheck) {
            $this->r_data['success'] = 0;
            $this->r_data['message'] = 'Số điện thoại đã tồn tại!';
            return $this->r_data;
        }

        $this->r_data['success'] = 1;
        $this->r_data['message'] = 'Email mobile verify success';
        return $this->r_data;
    }

    /**
     * Rider Register
     */
    public function rider_register($data)
    {

        $this->load->model('common_model');
        $user_data = array(
            'usertype' => isset($data->usertype) ? $data->usertype : '',
            'firstname' => isset($data->firstname) ? $data->firstname : '',
            'email' => isset($data->email) ? $data->email : '',
            'password' => isset($data->password) ? md5($data->password) : '',
            'platform' => isset($data->platform) ? $data->platform : '',
            'deviceid' => isset($data->deviceid) ? $data->deviceid : '',
            'contactno' => isset($data->contactno) ? $data->contactno : '',
            'address' => isset($data->address) ? $data->address : '',
            'dob' => isset($data->dob) ? $data->dob : '',
            'lat' => isset($data->lat) ? $data->lat : '',
            'lng' => isset($data->lng) ? $data->lng : '',
            'is_verify' => '0',
            'online_status' => 'offline',
            'country' => isset($data->country) ? $data->country : '',
            'state' => isset($data->state) ? $data->state : '',
            'city' => isset($data->city) ? $data->city : '',
        );

        if (!empty($data->profilepic)) {
            $user_data['profilepic'] = $data->profilepic;
        }

        $user_data['contactno'] = str_replace(["(", ")", "-", " "], "", $user_data['contactno']);
        $user_data['contactno'] = preg_replace("/^\+?(84)/", "0", $user_data['contactno']);

        $userdata = array('usertype', 'firstname');
        $validation = $this->paramValidation($userdata, $user_data);
        if ($validation['success'] == 0) {
            return $this->r_data;
        }

        $this->load->model('user_model');

        $chars = "0123456789";
        $code = $user_data['code'] = substr(str_shuffle($chars), 0, 4);
        $existsUser = $this->user_model->getExistsUser($user_data['contactno'], $data->usertype);
        if (!empty($existsUser)) {
            if ($existsUser->is_verify) {
                $this->r_data['success'] = 0;
                $this->r_data['message'] = 'Số điện thoại đã đăng ký với hệ thống';
                return $this->r_data;
            } else {
                $this->user_model->deleteUser($existsUser->id);
            }
        }

        $verifycode = $this->common->getSecureKey();

        $user_data['emailverify'] = $verifycode;
        $userid = $this->user_model->addUser($user_data);
        //Add user session
        $this->r_data['token'] = $this->common->getSecureKey();
        $user_session = array(
            'userid' => $userid,
            'token' => $this->r_data['token'],
            'login_type' => 'verify',
            'start_date' => DATETIME,
        );
        $this->common->insertUserSession($user_session);

        $this->r_data['success'] = 1;
        $this->r_data['code'] = $code;
        $this->r_data['userid'] = $userid;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, otp_api);

        $data = [
            'username' => otp_username,
            'password' => otp_pass,
            'phones' => [$data->contactno],
            'smsContent' => 'Ma OTP cua ban la ' . $code,
            'reqid' => otp_reqid
        ];
        $headers = array(
            'Content-Type:application/json'
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        curl_exec($ch);
        curl_close($ch);
        return $this->r_data;
    }

    /**
     * Verify User
     */
    public function verify_user($code)
    {
        $this->load->model('user_model');
        $user_details = $this->user_model->getUserWithSession($this->token_id, 'verify');
        $verify = false;
        if ($user_details) {
            if ($code && $user_details->code == $code) {
                $verify = true;
                $this->user_model->updateUser(array('is_verify' => 1, 'code' => 0), $this->userid);
                $this->common->logoutUser($user_details->session_id);
            }
        }

        if ($verify) {
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Successfully verified.';
            $this->r_data['data'] = $this->user_model->getUser($this->userid);
        } else {
            $this->r_data['message'] = 'User not verified.';
        }
        return $this->r_data;
    }

    /**
     * Resend Code
     */
    public function resend_code($contactno)
    {
        $this->load->model('user_model');
        $user_details = $this->user_model->checkUserExists('contactno', $contactno, 'driver');
        $this->r_data['message'] = 'User already verified.';
        if (!$user_details) {
            $this->r_data['message'] = 'User not found.';
        }

        $user_data = $this->user_model->getUserWithSession($this->token_id, 'verify');
        if ($user_data && $user_details && $user_details->is_verify == 0) {
            $chars = "0123456789";
            $code = substr(str_shuffle($chars), 0, 4);
            $this->user_model->updateUser(array('code' => $code), $this->userid);
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, otp_api);

            $data = [
                'username' => otp_username,
                'password' => otp_pass,
                'phones' => [$contactno],
                'smsContent' => 'Ma OTP cua ban la ' . $code,
                'reqid' => otp_reqid
            ];
            $headers = array(
                'Content-Type:application/json'
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

            curl_exec($ch);
            curl_close($ch);
            $this->r_data['success'] = 1;
            $this->r_data['code'] = $code;
            $this->r_data['message'] = 'Verification code successfully sent.';
        }
        return $this->r_data;
    }

    /**
     * Update Location
     */
    public function updateLocation($userid, $clat, $clng)
    {
        $this->load->model('user_model');
        $user_details = $this->user_model->getUser($userid);
        if (!$user_details) {
            $this->r_data['message'] = 'User not found.';
        }

        if ($user_details) {
            $clat = str_replace(":", "", $clat);

            $this->user_model->updateLocation($userid, $clat, $clng);
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Location Update successfully';
        }
        return $this->r_data;
    }

    /**
     * Get All Driver Order History
     */
    public function get_rider_pending_orders_history()
    {
        $this->load->model('user_model');
        $this->load->model('order_model');
        $this->r_data['is_assigned'] = "0";
        $this->r_data['message'] = 'No Pending Order';
        $user_data = $this->user_model->getUser($this->userid);
        $orders = $this->order_model->getRiderPendingOrders($this->userid);
        $isAssigned = $this->order_model->getRiderIsAssigned($this->userid);
        if (!empty($isAssigned) && $isAssigned->is_assigned > 0) {
            $this->r_data['is_assigned'] = "1";
        }
        if ($user_data->online_status == 'offline') {
            return $this->r_data;
        }
        if ($orders) {
            $this->r_data['data'] = $orders;
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Get Pending Order successfully';
        }
        return $this->r_data;
    }

    /**
     * Get Driver Assigned Order History
     */
    public function get_rider_assign_orders()
    {
        $this->load->model('order_model');
        $orders = $this->order_model->getRiderAssignedOrders($this->userid);
        //echo $this->db->last_query();
        $this->r_data['is_assigned'] = "0";
        $this->r_data['message'] = 'No Order Assigned.';
        $isAssigned = $this->order_model->getRiderIsAssigned($this->userid);
        if (!empty($isAssigned) && $isAssigned->is_assigned > 0) {
            $this->r_data['is_assigned'] = "1";
        }
        if ($orders) {
            $this->r_data['data'] = $orders;
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Get Assign Order successfully';
        }
        return $this->r_data;
    }

    /*
     * get complete order list
     */

    public function get_complete_orders_list()
    {
        $this->load->model('order_model');
        $this->r_data['success'] = 0;
        $this->r_data['message'] = 'Order history Not Found..!';
        $this->r_data['is_assigned'] = "0";
        $orders = $this->order_model->getRiderCompletedOrders($this->userid);
        if ($orders) {
            $isAssigned = $this->order_model->getRiderIsAssigned($this->userid);
            if (!empty($isAssigned) && $isAssigned->is_assigned > 0) {
                $this->r_data['is_assigned'] = "1";
            }
            $this->r_data['data'] = $orders;
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Get Complete Order successfully';
        }
        return $this->r_data;
    }

    /**
     * Get Order Details
     */
    public function get_order_details($orderid, $type)
    {
        $redis_enable = config_item('redis_enable');
        if ($redis_enable === true) {
            $this->load->library('redis_client');
            $changed = $this->redis_client->getChangedStatus($orderid);
//            log_message('debug', 'CHANGED:' . $changed);
            if (!empty($changed) && ($changed === false || $changed === "false") ) {
                log_message('info', "Order Detail has NOT been changed:" . $orderid);
                $response = $this->redis_client->getOrderDetails($orderid, $type);
                if (!empty($response)) {
                    // return $response;
                    return [
                        'data' => $response,
                        'success' => 1,
                        'message' => 'Order Detail found successfully',
                        'is_assigned' => isset($response->is_assigned) ? $response->is_assigned : 0,
                    ];

                } else {
                    $response = $this->getOrderDetails($orderid, $type); //get from db
                    //cached
                    if (!empty($response['data'])) {
                        $this->redis_client->setOrderDetails($response['data'], $type);
                    }
                    return $response;
                }
            } else { // changed = true or not in cached
                log_message('info', "Order Detail has been changed OR not cached:" . $orderid);
                $response = $this->getOrderDetails($orderid, $type); //get from db
                if (!empty($response['data'])) {

                    //cached
                    $this->redis_client->setOrderDetails($response['data'], $type);
                    $this->redis_client->setChangedOrderDetail($orderid, false);
                }
                return $response;
            }
        } else {
            return $this->getOrderDetails($orderid, $type);
        }

    }

    private function getOrderDetails($orderid, $type)
    {
        $this->load->model('order_model');
        $this->load->model('dbcommon');
        $order = $this->order_model->getOrderDetails($orderid);
        $this->r_data['message'] = 'Đơn hàng không còn khả dụng, vui lòng cập nhật lại.';
        if ($order) {
            $order->fare_amount = $order->user_cash_payment;
            $order->images = [];

            $this->r_data['data'] = $order;
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Order Detail found successfully';
        }
        log_message('info', json_encode($this->r_data));
        return $this->r_data;
    }
    /**
     * Get User Profile
     */
    public function get_user_profile()
    {
        $this->load->model('user_model');
        $user_data = $this->user_model->getUser($this->userid);
        $this->r_data['message'] = 'User detail not found.';
        if (empty($user_data)) {
            header('HTTP/1.1 400 Unauthorize', true, 400);
        }

        if ($user_data) {
            $user_data->userid = $user_data->id;
            $this->r_data['data'] = $user_data;
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'User detail found.';
        }
        return $this->r_data;
    }

    /**
     * change password
     */
    public function changePassword($oldpassword, $newpassword, $confirmpassword)
    {
        $this->load->model('user_model');

        if ($newpassword != $confirmpassword) {
            $this->r_data['message'] = 'Password and confirm password mismatch.';
            return $this->r_data;
        }

        $user_data = $this->user_model->getUser($this->userid);
        $this->r_data['message'] = 'User detail not found.';
        if ($user_data) {
            if ($user_data->password != md5($oldpassword)) {
                $this->r_data['message'] = 'Old Password Mismatch.';
                return $this->r_data;
            }
            $userUpdate = $this->user_model->updateUser(array('password' => md5($newpassword)), $this->userid);
            if (!$userUpdate) {
                $this->r_data['message'] = 'Change password failed.';
            }

            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Password changed successfully.';
        }
        return $this->r_data;
    }

    /**
     * change password
     */
    public function UpdateDevice($platform, $deviceid, $devicename = "", $deviceversion = "", $appversion = "")
    {
        $this->load->model('user_model');

        if (!empty($deviceid)) {
            $this->user_model->updateUser(array('platform' => $platform, 'deviceid' => $deviceid, 'devicename' => $devicename, 'deviceversion' => $deviceversion, 'appversion' => $appversion), $this->userid);
        }
        $this->r_data['success'] = 1;
        $this->r_data['message'] = 'Update Device Successfully.';
        return $this->r_data;
    }

    /**
     * User Place Order
     */
    public function placeOrder($data)
    {

        $this->load->model('user_model');
        $this->load->model('dbcommon');

        $userid = isset($data->userid) ? $data->userid : '';
        $lastorderid = isset($data->lastorderid) ? $data->lastorderid : '';
        $userresult = $this->user_model->getUser($userid);
        if ($userresult->status) {
            $this->r_data['message'] = 'Your account has been disabled.';
            return $this->r_data;
        }

        $order_data = array(
            'transaction_id' => !empty($data->transaction_id) ? $data->transaction_id : '',
            'merchant_id' => !empty($data->merchant_id) ? $data->merchant_id : '',
            'userid' => isset($data->userid) ? $data->userid : '',
            'rider_id' => '0',
            'ordertype' => isset($data->ordertype) ? $data->ordertype : '',
            'shipping_amount' => isset($data->user_cash_payment) ? $data->user_cash_payment : '',
            'pickup_amount' => isset($data->order_total) ? $data->order_total : 0,
            'dropoff_amount' => isset($data->order_total) ? $data->order_total : 0,
            'user_cash_payment' => isset($data->user_cash_payment) ? $data->user_cash_payment : '',
            'cash_payment' => isset($data->cash_payment) ? $data->cash_payment : '',
            'pickup_address' => isset($data->pickup_address) ? $data->pickup_address : '',
            'plat' => isset($data->plat) ? $data->plat : '',
            'plng' => isset($data->plng) ? $data->plng : '',
            'pickup_city' => isset($data->pickup_city) ? $data->pickup_city : '',
            'pickup_state' => isset($data->pickup_state) ? $data->pickup_state : '',
            'pickup_country' => isset($data->pickup_country) ? $data->pickup_country : '',
            'pickup_postal' => isset($data->pickup_postal) ? $data->pickup_postal : '',
            'destination_address' => isset($data->destination_address) ? $data->destination_address : '',
            'dlat' => isset($data->dlat) ? $data->dlat : '',
            'dlng' => isset($data->dlng) ? $data->dlng : '',
            'drop_city' => isset($data->drop_city) ? $data->drop_city : '',
            'drop_state' => isset($data->drop_state) ? $data->drop_state : '',
            'drop_country' => isset($data->drop_country) ? $data->drop_country : '',
            'drop_postal' => isset($data->drop_postal) ? $data->drop_postal : '',
            'delivery_address' => isset($data->delivery_address) ? $data->delivery_address : '',
            'dllat' => isset($data->dllat) ? $data->dllat : '',
            'dllng' => isset($data->dllng) ? $data->dllng : '',
            'no_of_km' => isset($data->no_of_km) ? $data->no_of_km : '',
            'delivery_type' => isset($data->delivery_type) ? $data->delivery_type : '',
            'description' => isset($data->description) ? strip_tags($data->description) : '',
            'images' => isset($data->images) ? $data->images : '',
            'promocode' => isset($data->promocode) ? $data->promocode : '',
            'promoamount' => isset($data->promoamount) ? $data->promoamount : '',
            'tips' => isset($data->tipamt) ? $data->tipamt : '',
            'sender_name' => isset($data->sender_name) ? $data->sender_name : '',
            'recipient_name' => isset($data->recipient_name) ? $data->recipient_name : '',
            'courier_service_name' => isset($data->courier_service_name) ? $data->courier_service_name : '',
            'orderdate' => date('Y-m-d H:i:s', time()),
            'status' => "pending",
            'platform' => isset($data->platform) ? $data->platform : '',
            'is_rain' => isset($data->is_rain) ? $data->is_rain : '0',
            'merchant_contact' => isset($data->merchant_contact) ? $data->merchant_contact : '',
            'customer_contact' => isset($data->customer_contact) ? $data->customer_contact : '',
            'order_items' => isset($data->order_items) ? $data->order_items : '',
            'order_total' => isset($data->order_total) ? $data->order_total : 0,
            'merchant_order_id' => isset($data->order_id) ? $data->order_id : '',
            'extra' => isset($data->extra) ? $data->extra : '',
            'customer_contact_2' => isset($data->cusomer_contact_2) ? $data->cusomer_contact_2 : '',

        );
        $orderdata = array('userid', 'user_cash_payment', 'pickup_address', 'pickup_address', 'plat', 'plng', 'status');
        $validation = $this->paramValidation($orderdata, $order_data);
        if ($validation['success'] == 0) {
            return $this->r_data;
        }
        if ($order_data["no_of_km"] <= 0) {
            $this->r_data['message'] = 'The location you select have problem. Please select another location.';
            $this->r_data['success'] = 0;
            return $this->r_data;
        }

        $orderid = $this->user_model->addOrder($order_data);

        /*if (!empty($referralcode) && !empty($orderid)) {
        $this->wallet_transaction($referralcode->userid, $orderid, 'credit', '6','Place new order');
        }*/
        $this->r_data['success'] = 1;
        $this->r_data['orderid'] = $orderid;
        $this->r_data['message'] = 'Order Placed Successfully.';
        return $this->r_data;
    }

    public function placeorderNotification($orderid, $lat = '0.0', $lng = '0.0', $radius = 10)
    {
        $timeLimit = config_item('rider_time_limit_scanning');

        $this->load->model('dbcommon');
        $this->load->model('user_model');
        $this->load->model('common_model');
        $this->load->model('order_model');
        $order_data = $this->order_model->getById($orderid);
        if (!empty($order_data) && $order_data->status == 'Pending') {
            $rider_searching_init_radius = config_item('rider_searching_init_radius', 15);
            $rider_searching_increase_radius = config_item('rider_searching_increase_radius', 0);
            $rider_searching_max_round = config_item('rider_searching_max_round', 1);
            $rider_searching_max_radius = config_item('rider_searching_max_radius', 15);
            $rider_searching_resend_notification = config_item('rider_searching_resend_notification', 0);
            $current_round = $order_data->current_round ? $order_data->current_round : 0;
            $radius = $rider_searching_init_radius + ($rider_searching_increase_radius * $current_round);
            $current_round += 1;

            if ($radius <= $rider_searching_max_radius && $current_round <= $rider_searching_max_round) {

                $select_query = "SELECT u.id,firstname,lastname,lat,lng,platform,deviceid, n.id as notification_id,(SELECT COUNT(id) FROM order_master WHERE rider_id = u.id AND `riderstatus` NOT IN ('Complete','PickupDone')) AS freerider,(1.60934 * 3959 * acos(cos(radians(" . $lat . ")) * cos(radians(lat)) * cos( radians(lng) - radians(" . $lng . ")) + sin(radians(" . $lat . ")) * sin(radians(lat)))) AS distance FROM user_master as u LEFT JOIN notification_master n ON (n.riderid = u.id AND n.order_id = " . $orderid . " AND n.type = 'neworder')";
                $where_query = " WHERE usertype='driver' AND online_status='online' AND is_verify='1' AND '" . date('Y-m-d H:i:s', time()) . "' < DATE_ADD( u.modify_date, INTERVAL " . $timeLimit . " SECOND) AND can_receive_order = 1";
                if ($rider_searching_resend_notification == 0) {
                    $where_query .= " AND (n.id IS NULL OR n.id = 0)";
                } else {
                    $where_query .= " AND (n.isbided IS NULL OR n.isbided = 0)";
                }
                $having_query = " HAVING (distance < " . $radius . " AND freerider = 0) ORDER BY `distance` ASC";
                $query_near = $select_query . $where_query . $having_query;
                log_message('debug', $query_near);
                $drivers = $this->dbcommon->getInfo_($query_near, 1);

                $updateData = array('current_round' => $current_round, 'distance' => $radius);
                $this->order_model->updateOrderById($updateData, $orderid);

                if (!empty($drivers)) {

                    $iosdeviceids = $and_ids = array();
                    $userresult = $this->user_model->getUser($this->userid);

                    $msgDb = "<strong>" . $userresult->firstname . "</strong> đã đặt đơn hàng mới. Vui lòng kiểm tra và nhận đơn. Mã đơn hàng: " . $order_data->merchant_order_id;
                    $ntmsg = $userresult->firstname . " đã đặt đơn hàng mới. Vui lòng kiểm tra và nhận đơn. Mã đơn hàng: " . $order_data->merchant_order_id;
                    foreach ($drivers as $val) {
                        $noti_data = array(
                            "userid" => $val->id,
                            "riderid" => $val->id,
                            "message" => $msgDb,
                            "order_id" => $orderid . "",
                            "type" => "neworder",
                            "status" => "neworder",
                            "createdate" => date('Y-m-d H:i:s', time()),
                        );
                        if (empty($val->notification_id)) {
                            $this->user_model->saveNotification($noti_data);
                        }
                        $noti_data['ordertype'] = $order_data->ordertype;
                        $message = array("message" => $ntmsg, "data" => $noti_data, "mtype" => "neworder");
                        $msg = array('title' => 'Thông báo', 'body' => $ntmsg);
                        if ($val->platform == 'android' && !empty($val->deviceid)) {
                            $and_ids[] = $val->deviceid;
                        }
                        if ($val->platform == 'ios' && !empty($val->deviceid)) {
                            $iosdeviceids[] = $val->deviceid;
                        }
                    }

                    if (!empty($and_ids)) {
                        $this->common_model->sendPushNotification($and_ids, $message, $msg, 'android');
                        $this->r_data['notification'] = 1;
                    }
                    if (!empty($iosdeviceids)) {
                        $this->common_model->sendPushNotification($iosdeviceids, $message, $msg, 'ios');
                        $this->r_data['notification'] = 1;
                    }
                    $this->r_data['success'] = 1;
                    $this->r_data['message'] = 'Place Order Notification Successfully';

                } else {
                    if ($order_data->platform == 'api' && ($current_round >= $rider_searching_max_round || $radius > $rider_searching_max_radius)) {
                        $queryTransaction = 'SELECT * FROM transactions WHERE order_id = ' . $orderid;
                        $transaction = $this->dbcommon->getInfo_($queryTransaction);
                        if (!empty($transaction)) {
                            $finvietData = [
                                'orderId' => $order_data->merchant_order_id,
                                'transactionId' => $transaction->id,
                                'status' => 0,
                                'statusCode' => 'FAILED',
                                'riderId' => 0,
                                'riderName' => '',
                                'expected_arrival' => '',
                                'expected_delivery' => ''
                            ];
//                            $this->finviet_notification('riderStatus', $finvietData);
                        } else {
                            log_message('info', 'Finviet Transaction Not Fouund for order' . $orderid);
                        }
                        $this->r_data['success'] = 0;
                        $this->r_data['code'] = 1100;
                        $this->r_data['message'] = 'Đơn hàng không còn khả dụng, vui lòng cập nhật lại.';
                    } else {
                        $this->r_data['success'] = 0;
                        $this->r_data['code'] = 1210;
                        $this->r_data['message'] = 'Không tìm thấy tài xế';
                    }
                }
            } else {
                $queryTransaction = 'SELECT * FROM transactions WHERE order_id = ' . $orderid;
                $transaction = $this->dbcommon->getInfo_($queryTransaction);
                if (!empty($transaction)) {
                    $finvietData = [
                        'orderId' => $order_data->merchant_order_id,
                        'transactionId' => $transaction->id,
                        'status' => 0,
                        'statusCode' => 'FAILED',
                        'riderId' => 0,
                        'riderName' => '',
                        'expected_arrival' => '',
                        'expected_delivery' => ''
                    ];
                    //$this->finviet_notification('riderStatus', $finvietData);
                } else {
                    log_message('info', 'Finviet Transaction Not Fouund for order' . $orderid);
                }
                $this->r_data['success'] = 0;
                $this->r_data['code'] = 1100;
                $this->r_data['message'] = 'Đơn hàng không còn khả dụng, vui lòng cập nhật lại.';
            }
        } else {
            $this->r_data['success'] = 0;
            $this->r_data['code'] = 1100;
            $this->r_data['message'] = 'Đơn hàng không còn khả dụng, vui lòng cập nhật lại.';
        }
        return $this->r_data;
    }

    /**
     * Update Profile
     */
    public function updateProfile($userid, $firstname, $address, $dob = '', $contactno, $email = '', $referralcode = '', $state='')
    {
        $this->load->model('user_model');
        $this->load->model('dbcommon');
        $user_details = $this->user_model->getUser($userid);
        if (!$user_details) {
            $this->r_data['message'] = 'User not found.';
        }

        if ($user_details) {
            $emailCount = '0';
            if ($email) {
                $emailCountQuery = "* FROM user_master WHERE email='" . $email . "' AND usertype='" . $user_details->usertype . "'";
                $emailCount = $this->dbcommon->getnumofdetails_($emailCountQuery);
            }
            if ($emailCount > 2) {
                $this->r_data['success'] = 0;
                $this->r_data['message'] = 'Emailid Already Exist..!';
                $this->r_data['data'] = $user_details;
            } else {
                $dataArray = array('firstname' => $firstname, 'address' => $address, 'contactno' => $contactno);
                if (!empty($dob)) {
                    $dataArray['dob'] = $dob;
                }
                $dataArray['email'] = $email;
                if (!empty($state)) {
                    $dataArray['state'] = $state;
                }
                $this->user_model->updateUser($dataArray, $this->userid);
                $user_data = $this->user_model->getUser($this->userid);

                $orders = $this->user_model->getuserOrder($this->userid);
                $user_data->isorder = true;
                if (empty($orders)) {
                    $user_data->isorder = false;
                }
                $user_data->userid = $user_data->id;
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'Profile Update successfully';
                $this->r_data['data'] = $user_data;
            }
        }
        return $this->r_data;
    }

    /**
     * change profile pic
     */
    public function updateProfilepic($userid, $profilepic)
    {
        $this->load->model('user_model');
        $user_details = $this->user_model->getUser($userid);
        if (!$user_details) {
            $this->r_data['message'] = 'User not found.';
        }

        if ($user_details) {
            if (!empty($profilepic)) {
                $dataArray['profilepic'] = $profilepic;
            }
            $this->user_model->updateUser($dataArray, $this->userid);
            $user_data = $this->user_model->getUser($this->userid);
            $orders = $this->user_model->getuserOrder($this->userid);
            $user_data->isorder = true;
            if (empty($orders)) {
                $user_data->isorder = false;
            }
            $user_data->userid = $user_data->id;
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Profile pic Update successfully';
            $this->r_data['data'] = $user_data;
        }
        return $this->r_data;
    }

    /**
     * Get Notification
     */
    public function get_notification($page = 0)
    {
        $this->load->model('user_model');
        //echo $this->userid;die;
        $udata = $this->user_model->getUser($this->userid);
        if (!empty($udata)) {
            $type = $udata->usertype;
//            $updata = array("isview" => 1);
//            $this->user_model->updatetabnoti($this->userid, $type, $updata);
            $user_data = $this->user_model->getNotification($this->userid, $type, $page);
            $this->r_data['message'] = 'No Notification.';
            if ($user_data) {

                $this->r_data['count'] = $user_data['count'];
                $this->r_data['data'] = $user_data['data'];
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'Get Notification.';
            }
        } else {
            $this->r_data['success'] = 0;
            $this->r_data['message'] = 'Empty Notification.';
        }
        return $this->r_data;
    }
    /*
     * get rider profile
     */

    public function get_rider_profile($riderid, $orderid)
    {
        $this->load->model('user_model');
        $riderdata = $this->user_model->getRiderwithorder($riderid, $orderid);
        $this->r_data['message'] = 'No Rider Data Found..!';
        if ($riderdata) {

            $imagedata = array();
            $images = explode(",", $riderdata->profilepic);

            for ($j = 0; $j < count($images); $j++) {
                if (!empty($images[$j])) {
                    $riderdata->profilepic = base_url . 'images/profile/' . $images[$j];
                }
            }

            $lat = $riderdata->lat;
            $lng = $riderdata->lng;
            $storelat = $riderdata->plat;
            $storelng = $riderdata->plng;
            $theta = $storelng - $lng;
            $dist = sin(deg2rad($storelat)) * sin(deg2rad($lat)) + cos(deg2rad($storelat)) * cos(deg2rad($lat)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = 'K';
            $unit = strtoupper($unit);
            if ($unit == "K") {
                $distance = round(($miles * 1.609344), 1);
            }
            $riderdetails = $riderdata;
            $satisfaction = $this->user_model->getIndivisualRatesOFRider($riderid, 'satisfaction');

            if (!empty($satisfaction->rates)) {
                $riderdetails->satisfaction_rate = ROUND($satisfaction->rates, 1);
            } else {
                $riderdetails->satisfaction_rate = 5;
            }
            $communication = $this->user_model->getIndivisualRatesOFRider($riderid, 'communication');
            if (!empty($communication->rates)) {
                $riderdetails->communication_rate = ROUND($communication->rates, 1);
            } else {
                $riderdetails->communication_rate = 5;
            }
            $ontime = $this->user_model->getIndivisualRatesOFRider($riderid, 'ontime');
            if (!empty($ontime->rates)) {
                $riderdetails->ontime_rate = ROUND($ontime->rates, 1);
            } else {
                $riderdetails->ontime_rate = 5;
            }
            $riderdetails->distance = $distance;
            $this->r_data['data'] = $riderdetails;
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Get Rider Data Successfully..!';
        }
        return $this->r_data;
    }

    /*
     * change online offline status
     */

    public function change_live_status($status)
    {
        $this->load->model('user_model');
        $this->r_data['success'] = 0;
        $this->r_data['message'] = 'User Not Found';
        if ($status) {
            $statusData = array('online_status' => $status);
            $update = $this->user_model->updateUser($statusData, $this->userid);
            if ($update) {
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'Status Change Successfully';
            }
        }
        return $this->r_data;
    }
    /**
     * get all message list
     */
    public function getMessagelist()
    {
        $userid = $this->userid;
        $this->load->model('user_model');
        $data = [];
        $this->r_data['message'] = "No messages found";
        if (!empty($data)) {
            $this->r_data['data'] = $data;
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Get Message Successfully';
        }
        return $this->r_data;
    }

    /**
     * Post Send Message
     */
    public function sendmessage($data)
    {

        $this->load->model('dbcommon');
        $message_data = array(
            'orderid' => isset($this->orderid) ? $this->orderid : '',
            'fromId' => isset($this->userid) ? $this->userid : '',
            'toId' => isset($data->toId) ? $data->toId : '',
            'message' => isset($data->message) ? $data->message : '',
            'attachment' => isset($data->attachment) ? $data->attachment : '',
            'create_date' => date('Y-m-d H:i:s', time()),
        );

        $messagedata = array('toId');
        $validation = $this->paramValidation($messagedata, $message_data);
        if ($validation['success'] == 0) {
            return $this->r_data;
        }

        $this->load->model('message_model');
        $result = $this->message_model->insertMessage($message_data);

        if ($result) {

            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Message Send Successfully.';
            $this->r_data['data'] = $message_data;
            return $this->r_data;
        } else {
            $this->r_data['success'] = 0;
            $this->r_data['message'] = 'Receiver user not exist';
            return $this->r_data;
        }
    }

    /**
     * Post Get conversation
     */
    public function getconversation($data)
    {
        $message_data = array(
            'fromId' => isset($this->userid) ? $this->userid : '',
            'toId' => isset($data->toId) ? $data->toId : '',
            'mid' => isset($data->mid) ? $data->mid : '',
        );

        $messagedata = array('fromId', 'toId');
        $validation = $this->paramValidation($messagedata, $message_data);
        if ($validation['success'] == 0) {
            return $this->r_data;
        }

        $this->load->model('message_model');
        $messagedata = $this->message_model->getUserConversation($this->userid, $data->toId);

        $this->r_data['success'] = 1;
        $this->r_data['message'] = 'Get Conversation Successfully.';
        $this->r_data['data'] = $messagedata;
        return $this->r_data;
    }


    public function feedback($orderid = '', $subject = '', $description)
    {
        $this->load->model('user_model');
        $this->r_data['success'] = 0;
        $userid = $this->userid;

        if ($orderid != '') {

            $data = array('complainer_id' => $userid, 'order_id' => $orderid, 'subject' => $subject, 'complain' => $description, 'status' => '0', 'create_date' => date('Y-m-d H:i:s', time()));
            $insertid = $this->user_model->addComplain($data);
        } else {

            $data = array('user_id' => $userid, 'subject' => $subject, 'description' => $description, 'status' => '0', 'create_date' => date('Y-m-d H:i:s', time()));
            $insertid = $this->user_model->addfeedback($data);
        }

        if ($insertid) {
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Feedback Sent Successfully';
        } else {
            $this->r_data['success'] = 0;
            $this->r_data['message'] = 'Something Went wrong';
        }

        return $this->r_data;
    }

    /*
     * rider give the feedback to user
     */

    public function give_feedback_to_user($data)
    {
        $this->load->model('dbcommon');
        $this->load->model('common_model');
        $this->load->model('user_model');
        $this->load->model('order_model');
        $userid = isset($data->userid) ? $data->userid : '';
        $orderid = isset($data->orderid) ? $data->orderid : '';
        $riderid = isset($data->riderid) ? $data->riderid : '';
        $this->r_data['success'] = 0;
        $this->r_data['message'] = 'Đơn hàng không còn khả dụng, vui lòng cập nhật lại.';
        $userresult = $this->user_model->getUser($riderid);
        $order = $this->order_model->getById($orderid);
        if (!empty($order) && $order->rider_id == $riderid && $order->status == 'In Progress' && $order->is_dropoff == 2) {

//            $this->order_model->updateOrderShippingFee($orderid);
            $walletData = [
                'user_id' => $riderid,
                'order_id' => $orderid,
                'amount' => $order->user_cash_payment,
                'tip' => '',
            ];
            $this->user_model->insertWalletTransaction($walletData);
            $cwhere = array("userid" => $userid, "order_id" => $orderid);
            $notidata = $this->dbcommon->getalldetailsinfo($this->common->getNotificationTable(), $cwhere);
            if ($notidata) {
                foreach ($notidata as $val) {
                    $this->update_read_notification($val->id);
                }
            }
            $signature_image = isset($data->signature_image) ? $data->signature_image : '';
            $data = array('riderstatus' => "Complete", 'signature_image' => $signature_image, 'delivery_time' => date('Y-m-d H:i:s', time()), 'status' => 'Complete');
            $result = $this->order_model->updateOrderById($data, $orderid);
            if ($result) {
                $ntmsg = "Bạn vừa giao thành công đơn hàng " . $order->merchant_order_id;
                $noti_data = array(
                    "userid" => $riderid,
                    "riderid" => $riderid,
                    "message" => $ntmsg,
                    "order_id" => $orderid,
                    "status" => 'complete_by_rider',
                    "createdate" => date('Y-m-d H:i:s', time()),
                );
                $notification = $this->user_model->saveNotification($noti_data);

                $noti_data['ordertype'] = $order->ordertype;
                $noti_data['userstatus'] = $order->status;
                $noti_data['riderstatus'] = $order->riderstatus;

                $message = array("message" => $ntmsg, "data" => $noti_data, "mtype" => "complete_by_rider");
                //$messageios = $ntmsg;
                $msg = array('title' => 'Thông báo', 'body' => $ntmsg);
                $iosdeviceids = $and_ids = array();

                if ($userresult->platform == 'ios' && !empty($userresult->deviceid)) {
                    $iosdeviceids[] = $userresult->deviceid;
                } else if ($userresult->platform == 'android' && !empty($userresult->deviceid)) {
                    $and_ids[] = $userresult->deviceid;
                }
                if (!empty($and_ids)) {
                    $this->common_model->sendPushNotification($and_ids, $message, $msg, 'android');
                    $this->r_data['notification'] = 1;
                }
                if (!empty($iosdeviceids)) {
                    $this->common_model->sendPushNotification($iosdeviceids, $message, $msg, 'ios');
                    $this->r_data['notification'] = 1;
                }
                if ($order->platform == 'api' && empty($order->is_return)) {
                    $queryTransaction = 'SELECT * FROM transactions WHERE order_id = ' . $orderid;
                    $transaction = $this->dbcommon->getInfo_($queryTransaction);
                    if (!empty($transaction)) {
                        $finvietData = [
                            'orderId' => $order->merchant_order_id,
                            'transactionId' => $transaction->id,
                            'riderId' => $riderid,
                            'riderName' => $userresult->firstname,
                            'riderContact' => $userresult->contactno,
                            'riderWallet' => $userresult->contactno,
                            'riderAvatar' => $userresult->profilepic ? $userresult->profilepic : no_image_default,
                            'riderLatitude' => $userresult->lat,
                            'riderLongitude' => $userresult->lng,
                            'status' => 3,
                            'statusCode' => 'COMPLETED'
                        ];
                        $this->finviet_notification('deliveryStatus', $finvietData);
                    } else {
                        log_message('info', 'Finviet Transaction Not Fouund for order' . $orderid);
                    }
                }

                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'Feedback Sent Successfully..!';
            }
        } else if ($order->status == 'Complete') {
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Feedback Sent Successfully..!';
        }

        return $this->r_data;
    }


    /*
     * update read notifications
     */

    public function update_read_notification($notificationId=0)
    {
        $this->load->model('user_model');
        $this->load->model('dbcommon');

        $updata = array("isview" => 1);

        $notiupdate = $this->user_model->updatetabnoti($this->userid, "driver", $updata, $notificationId);
        if ($notiupdate) {
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Notification read successfully...!';
        } else {
            $this->r_data['success'] = 0;
            $this->r_data['message'] = '';
        }
        return $this->r_data;
    }

    /*
     * update read notifications
     */

    public function update_bid_notification($notificationId=0)
    {
        $this->load->model('user_model');
        $this->load->model('dbcommon');

        $updata = array("isview" => 1, 'isbided' => 1);

        $notiupdate = $this->user_model->updatetabnoti($this->userid, "driver", $updata, $notificationId);
        if ($notiupdate) {
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Notification read successfully...!';
        } else {
            $this->r_data['success'] = 0;
            $this->r_data['message'] = '';
        }
        return $this->r_data;
    }

    /*
     * update read notifications
     */

    public function delete_notification($notification_id)
    {
        $this->load->model('user_model');

        $notiupdate = $this->user_model->deleteNotification($notification_id);
        if ($notiupdate) {
            $this->r_data['success'] = 1;
            $this->r_data['message'] = 'Notification deleted successfully...!';
        } else {
            $this->r_data['success'] = 0;
            $this->r_data['message'] = 'Something wrong..!';
        }
        return $this->r_data;
    }

    /**
     * Place Bid
     */
    public function postbid($orderid)
    {
        $this->load->model('user_model');
        $this->load->model('order_model');
        $this->load->model('dbcommon');
        $order_data = $this->order_model->getById($orderid);
        $query_near = "SELECT COUNT(id) AS freerider FROM order_master WHERE rider_id = '" . $this->userid . "' AND `riderstatus` IN ('Pending','PickupDone')";
        $drivers = $this->dbcommon->getInfo_($query_near);
        $this->r_data['success'] = 0;
        $this->r_data['message'] = 'Đơn hàng không còn khả dụng, vui lòng cập nhật lại.';
        if (isset($drivers->freerider) && empty($drivers->freerider)) {
            if (!empty($order_data) && empty($order_data->rider_id)) {
                if ($order_data->status != "Pending") {
                    return $this->r_data;
                }
                $checkdata = $this->user_model->Ckeckbided($orderid, $this->userid, $order_data->userid);
                if (empty($checkdata)) {

                    $riderid = $this->userid;
                    $orderData = array('rider_id' => $riderid, 'status' => 'In Progress', 'rider_accept_time' => date('Y-m-d H:i:s', time()));
                    $orderWhere = array('id' => $orderid, 'rider_id' => 0);
                    $update = $this->order_model->updateOrder($orderData, $orderWhere);
                    if ($update) {
                        $where = array("riderid" => $this->userid, "order_id" => $orderid, "status" => 'neworder');
                        $notidata = $this->dbcommon->getdetailsinfo($this->common->getNotificationTable(), $where);
                        if ($notidata) {
                            $this->update_bid_notification($notidata->id);
                        }
                        $userresult = $this->user_model->getUser($this->userid);
                        $noti_data = array(
                            "userid" => $order_data->userid,
                            "riderid" => $this->userid,
                            "message" => "Bạn vừa nhận giao một đơn hàng. Vui lòng hoàn thành.",
                            "order_id" => $orderid,
                            "isbided" => '1',
                            "type" => "bid",
                            "status" => "bid",
                            "createdate" => date('Y-m-d H:i:s', time()),
                        );
                        $this->user_model->saveNotification($noti_data);

                        //push notification
                        $this->load->model('common_model');

                        $where = array(
                            "userid" => $order_data->userid,
                            "riderid" => $riderid,
                            "order_id" => $orderid,
                            "status" => 'bid',
                        );
                        $noti_data = array(
                            "message" => "Bạn vừa nhận giao đơn hàng " . $order_data->merchant_order_id . ". Vui lòng hoàn thành.",
                            "status" => "assign_by_user",
                            "order_id" => $orderid,
                            "createdate" => date('Y-m-d H:i:s', time()),
                        );
                        $notidata = $this->dbcommon->getdetailsinfo($this->common->getNotificationTable(), $where);
                        if (!empty($notidata)) {
                            $notiupdate = $this->user_model->updateNoti($notidata->id, $noti_data);
                        }
                        $this->r_data['success'] = 1;
                        $this->r_data['message'] = 'Nhận đơn hàng thành công.';
                        // assign order
                        $noti_data['ordertype'] = $order_data->ordertype;

                        $ntmsg = 'Bạn vừa nhận giao đơn hàng ' . $order_data->merchant_order_id . '. Vui lòng hoàn thành.';
                        $message = array("message" => $ntmsg, "data" => $noti_data, "mtype" => "assign_by_user");
                        $msg = array ( 'title' => 'Thông báo', 'body' => $ntmsg );
                        $iosdeviceids = $and_ids = array();
                        $riderresult = $this->user_model->getUser($riderid);
                        if ($riderresult->platform == 'ios' && !empty($riderresult->deviceid)) {
                            $iosdeviceids[] = $riderresult->deviceid;
                        } else if ($riderresult->platform == 'android' && !empty($riderresult->deviceid)) {
                            $and_ids[] = $riderresult->deviceid;
                        }

                        if (!empty($and_ids)) {
                            $this->common_model->sendPushNotification($and_ids, $message,$msg,'android');
                            $this->r_data['notification'] = 1;
                        }
                        if (!empty($iosdeviceids)) {
                            $this->common_model->sendPushNotification($iosdeviceids, $message,$msg,'ios');
                            $this->r_data['notification'] = 1;
                        }
                        if ($order_data->platform == 'api') {
                            $queryTransaction = 'SELECT * FROM transactions WHERE order_id = ' . $orderid;
                            $transaction = $this->dbcommon->getInfo_($queryTransaction);
                            if (!empty($transaction)) {
                                $finvietData = [
                                    'orderId' => $order_data->merchant_order_id,
                                    'transactionId' => $transaction->id,
                                    'status' => 1,
                                    'statusCode' => 'PICKING_UP',
                                    'riderId' => $riderid,
                                    'riderName' => $userresult->firstname,
                                    'riderContact' => $userresult->contactno,
                                    'riderWallet' => $userresult->contactno,
                                    'riderAvatar' => $userresult->profilepic ? $userresult->profilepic : no_image_default,
                                    'riderLatitude' => $userresult->lat,
                                    'riderLongitude' => $userresult->lng,
                                    'expected_arrival' => '15 minutes',
                                    'expected_delivery' => ''
                                ];
                                $this->finviet_notification('riderStatus', $finvietData);
                            } else {
                                log_message('info', 'Finviet Transaction Not Fouund for order' . $orderid);
                            }
                        }
                    } else {
                        $this->r_data['success'] = 0;
                        $this->r_data['message'] = 'Đơn hàng không còn khả dụng, vui lòng cập nhật lại.';
                    }
                } else {
                    $this->r_data['success'] = 0;
                    $this->r_data['message'] = 'Bạn đã nhận đơn hàng này rồi';
                }
            } else {
                $this->r_data['success'] = 0;
                $this->r_data['message'] = 'Đơn hàng đã được chuyển cho tài xế khác.';
            }
        } else {
            $this->r_data['success'] = 0;
            $this->r_data['message'] = "Bạn đang nhận đơn hàng, vui lòng hoàn thành để có thể sử dụng chức năng này.";
        }
        return $this->r_data;
    }

    public function startpickup($orderid, $riderid)
    {
        $this->load->model('order_model');
        $this->load->model('dbcommon');
        $order_data = $this->order_model->getById($orderid);
        if (!empty($order_data) && $order_data->rider_id == $this->userid && $order_data->status == 'In Progress' && empty($order_data->is_pickup)) {
            $where = array("riderid" => $this->userid, "order_id" => $orderid, "status" => 'assign_by_user');
            $notidata = $this->dbcommon->getdetailsinfo($this->common->getNotificationTable(), $where);
            if ($notidata) {
                $this->update_read_notification($notidata->id);
            }
            $data = array('is_pickup' => 1, 'start_time' => date('Y-m-d H:i:s', time()));
            $result = $this->order_model->updateOrderById($data, $orderid);
            if ($result) {
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'Pickup Start Successfully...!';
            } else {
                $this->r_data['success'] = 0;
                $this->r_data['message'] = 'Something went Wrong..!';
            }
        } else {
            $this->r_data['success'] = 0;
            $this->r_data['message'] = 'Đơn hàng không còn khả dụng, vui lòng cập nhật lại.';
        }
        return $this->r_data;
    }

    public function repickupdone($riderid, $orderid)
    { //  for rider
        $this->load->model('user_model');
        $this->load->model('dbcommon');
        $this->load->model('order_model');

        $order = $this->order_model->getById($orderid);
        if (!empty($order) && $order->rider_id == $this->userid && $order->status == 'In Progress' && $order->is_pickup == 1) {
            $data = array('is_paid' => 2, 'is_pickup' => 3, 'riderstatus' => "PickupDone", 'pickup_time' => date('Y-m-d H:i:s', time()));
            $result = $this->order_model->updateOrderById($data, $orderid);
            if ($result) {
                $userresult = $this->user_model->getUser($this->userid);
                if ($order->platform == 'api') {
                    $queryTransaction = 'SELECT * FROM transactions WHERE order_id = ' . $orderid;
                    $transaction = $this->dbcommon->getInfo_($queryTransaction);
                    if (!empty($transaction)) {
                        $finvietData = [
                            'orderId' => $order->merchant_order_id,
                            'transactionId' => $transaction->id,
                            'riderId' => $this->userid,
                            'riderName' => $userresult->firstname,
                            'riderContact' => $userresult->contactno,
                            'riderWallet' => $userresult->contactno,
                            'riderAvatar' => $userresult->profilepic ? $userresult->profilepic : no_image_default,
                            'riderLatitude' => $userresult->lat,
                            'riderLongitude' => $userresult->lng,
                            'status' => 2,
                            'statusCode' => 'IN_DELIVERY'
                        ];
                        $this->finviet_notification('deliveryStatus', $finvietData);
                    } else {
                        log_message('info', 'Finviet Transaction Not Fouund for order' . $orderid);
                    }
                }
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'Pickup Successfully...!';
            } else {
                $this->r_data['success'] = 0;
                $this->r_data['message'] = 'Something went Wrong..!';
            }
        } else {
            $this->r_data['success'] = 0;
            $this->r_data['message'] = 'Đơn hàng không còn khả dụng, vui lòng cập nhật lại.';
        }
        return $this->r_data;
    }

    public function dropoff($riderid, $orderid, $is_return = 0)
    {
        $this->load->model('user_model');
        $this->load->model('dbcommon');
        $this->load->model('order_model');
        $this->r_data['success'] = 0;
        $this->r_data['message'] = 'Đơn hàng không còn khả dụng, vui lòng cập nhật lại.';
        $order = $this->order_model->getById($orderid);
        if (!empty($order) && $order->status == 'In Progress' && $order->rider_id == $this->userid && $order->is_pickup == 3 && empty($order->is_dropoff)) {
            $data = array(
                'dropoff_time' => date('Y-m-d H:i:s', time()),
                'is_dropoff' => 2,
                'total_amount' => $order->user_cash_payment,
                'is_return' => $is_return
            );
            $result = $this->order_model->updateOrderById($data, $orderid);
            if ($result) {
                if ($order->platform == 'api' && !empty($is_return)) {
                    $userresult = $this->user_model->getUser($order->rider_id);
                    $finvietData = [
                        'orderId' => $order->merchant_order_id,
                        'transactionId' => $order->transaction_id,
                        'riderId' => $order->rider_id ? $order->rider_id : 0,
                        'riderName' => $order->rider_id ? $userresult->firstname : "",
                        'riderContact' => $order->rider_id ? $userresult->contactno : "",
                        'riderWallet' => $order->rider_id ? $userresult->contactno : "",
                        'riderAvatar' => $order->rider_id ? ($userresult->profilepic ? $userresult->profilepic : no_image_default) : "",
                        'riderLatitude' => $order->rider_id ? $userresult->lat : 0,
                        'riderLongitude' => $order->rider_id ? $userresult->lng : 0,
                        'status' => 5,
                        'cancelReason' => 'Khách không nhận hàng',
                        'statusCode' => 'RETURNED'
                    ];
                    $this->finviet_notification('deliveryStatus', $finvietData);
                }
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'Dropoff successfully';
            } else {
                $this->r_data['success'] = 0;
                $this->r_data['message'] = 'Something goes wrong';
            }
        }
        return $this->r_data;
    }

    /*
     * re-order or cancel by customer
     */

    public function cancel_order($orderid, $reason="Hủy")
    {
        $this->load->model('user_model');
        $this->load->model('order_model');
        $this->load->model('dbcommon');
        $this->load->model('common_model');
        $this->r_data['success'] = 0;
        $this->r_data['message'] = 'Đơn hàng không còn khả dụng, vui lòng cập nhật lại.';
        $orderData = $this->order_model->getById($orderid);
        if ($orderData && $orderData->status != 'Canceled' && $orderData->status != 'Complete' && ($orderData->rider_id == $this->userid || $this->userid == $orderData->userid)) {
            $ntmsg = "Bạn vừa hủy đơn hàng " . $orderData->merchant_order_id;
            if ($orderData->rider_id != $this->userid) {
                $ntmsg = "Hệ thống vừa hủy đơn hàng " . $orderData->merchant_order_id;
            }
            $userresult = $this->user_model->getUser($orderData->rider_id);
            $noti_data = array(
                "userid" => $orderData->rider_id,
                "riderid" => $orderData->rider_id,
                "message" => $ntmsg,
                "order_id" => $orderid,
                "isbided" => '0',
                "type" => "cancelorder",
                "status" => "cancelorder",
                "createdate" => date('Y-m-d H:i:s', time()),
                "note" => $ntmsg . ' - Lý do: ' . $reason
            );
            $this->user_model->saveNotification($noti_data);

            $noti_data['ordertype'] = $orderData->ordertype;
            $message = array("message" => $ntmsg, "data" => $noti_data, "mtype" => "cancelorder");
            $msg = array('title' => 'Thông báo', 'body' => $ntmsg);
            $iosdeviceids = $and_ids = array();
            if (!empty($userresult)) {
                if ($userresult->platform == 'ios' && !empty($userresult->deviceid)) {
                    $iosdeviceids[] = $userresult->deviceid;
                } else if ($userresult->platform == 'android' && !empty($userresult->deviceid)) {
                    $and_ids[] = $userresult->deviceid;
                }
            }

            if (!empty($and_ids)) {
                $this->common_model->sendPushNotification($and_ids, $message, $msg, 'android');
                $this->r_data['notification'] = 1;
            }
            if (!empty($iosdeviceids)) {
                $this->common_model->sendPushNotification($iosdeviceids, $message, $msg, 'ios');
                $this->r_data['notification'] = 1;
            }

            $updateData = array('status' => 'Canceled', 'riderstatus' => 'Complete', "cancelby" => $this->userid, "cancel_reason" => $reason, "cancel_time" => date('Y-m-d H:i:s', time()));
            $update = $this->order_model->updateOrderById($updateData, $orderid);
            if ($update) {
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'You have canceled your order #' . $orderid;

                if ($orderData->platform == 'api') {
                    if ($orderData->rider_id != $this->userid || strtotime($updateData['cancel_time']) - strtotime($orderData->orderdate) > 60 * 10 || $orderData->is_pickup == 3) {
                        $finvietData = [
                            'orderId' => $orderData->merchant_order_id,
                            'transactionId' => $orderData->transaction_id,
                            'riderId' => $orderData->rider_id ? $orderData->rider_id : 0,
                            'riderName' => $orderData->rider_id ? $userresult->firstname : "",
                            'riderContact' => $orderData->rider_id ? $userresult->contactno : "",
                            'riderWallet' => $orderData->rider_id ? $userresult->contactno : "",
                            'riderAvatar' => $orderData->rider_id ? ($userresult->profilepic ? $userresult->profilepic : no_image_default) : "",
                            'riderLatitude' => $orderData->rider_id ? $userresult->lat : 0,
                            'riderLongitude' => $orderData->rider_id ? $userresult->lng : 0,
                            'status' => 5,
                            'cancelReason' => $reason
                        ];
                        if ($this->userid == $orderData->userid) {
                            $finvietData['statusCode'] = 'CANCELED';
                        } else {
                            if ($orderData->is_pickup != 3) {
                                $finvietData['statusCode'] = 'FAILED';
                            } else {
                                if ($this->userid == $orderData->rider_id) {
                                    $finvietData['statusCode'] = 'RETURNED';
                                } else {
                                    $finvietData['statusCode'] = 'CANCELED';
                                }
                            }
                        }
                        $this->finviet_notification('deliveryStatus', $finvietData);
                    } else {
                        $finvietData = [
                            'orderId' => $orderData->merchant_order_id,
                            'transactionId' => $orderData->transaction_id,
                            'riderId' => $orderData->rider_id ? $orderData->rider_id : 0,
                            'riderName' => $orderData->rider_id ? $userresult->firstname : "",
                            'riderContact' => $orderData->rider_id ? $userresult->contactno : "",
                            'riderWallet' => $orderData->rider_id ? $userresult->contactno : "",
                            'riderAvatar' => $orderData->rider_id ? ($userresult->profilepic ? $userresult->profilepic : no_image_default) : "",
                            'riderLatitude' => $orderData->rider_id ? $userresult->lat : 0,
                            'riderLongitude' => $orderData->rider_id ? $userresult->lng : 0,
                            'status' => 5,
                            'cancelReason' => $reason
                        ];
                        $finvietData['statusCode'] = 'FAILED';
                        $this->finviet_notification('deliveryStatus', $finvietData);
                        $data = array(
                            'status' => 'Pending',
                            'riderstatus' => 'Pending',
                            'rider_id' => 0,
                            'distance' => 0,
                            'current_round' => 0
                        );
                        $result = $this->order_model->updateOrderById($data, $orderid);
                        if ($result) {
                            $this->load->library('redis_client');
                            $token = $this->user_model->getUserToken($orderData->userid);
                            $notificationData = [
                                "token" => $token->token,
                                "user_id" => $orderData->userid,
                                "order_id" => $orderid,
                                "lat" => $orderData->plat,
                                "lng" => $orderData->plng,
                                "radius" => 5
                            ];
                            $this->redis_client->sendNewOrderNotification($notificationData);
                        }
                    }
                }
            } else {
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'Order Already Canceled';
            }
        }
        return $this->r_data;
    }
    /*
     * get Version
     */
    public function get_version()
    {
        $this->load->model('user_model');

        $setting = $this->user_model->getSettings();
        $this->r_data['customerapp'] = $setting->vcustomerapp;
        $this->r_data['riderapp'] = $setting->vriderapp;
        $this->r_data['success'] = 1;
        $this->r_data['message'] = 'get version Successfully';

        return $this->r_data;
    }

    /*
     * get Version
     */
    public function get_settings()
    {
        $this->load->model('user_model');

        $setting = $this->user_model->getSettings();
        $data['android_heremap_app_id'] = $setting->android_heremap_app_id;
        $data['android_heremap_app_code'] = $setting->android_heremap_app_code;
        $data['ios_heremap_app_id'] = $setting->ios_heremap_app_id;
        $data['ios_heremap_app_code'] = $setting->ios_heremap_app_code;
        $this->r_data['data'] = $data;
        $this->r_data['success'] = 1;
        $this->r_data['message'] = 'Get Settings Successfully';

        return $this->r_data;
    }

    public function test_api() {
        $this->load->model('user_model');
        $this->load->model('common_model');
        try {

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "http://sandbox.finviet.com.vn:9000/api/sms");

            $data = [
                'username' => '7tech',
                'password' => '111111',
                'phones' => ['0398608806'],
                'smsContent' => 'Ma OTP cua ban la 5730',
                'reqid' => '9374'
            ];
            $headers = array(
                'Content-Type:application/json'
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

            $response = curl_exec($ch);
            curl_close($ch);
            $this->r_data['response'] = $response;
        } catch(Exception $e) {

            trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);
            $this->r_data['error'] = $e->getMessage();
        }

        return $this->r_data;
    }
    
    public function finviet_notification($type, $data) {
        $ch = curl_init();

        if ($type == 'riderStatus' || $type == 'deliveryStatus') {
            curl_setopt($ch, CURLOPT_URL, config_item('finviet_notification_api') . $type);

            $headers = array(
                'Content-Type:application/json',
                'Authorization:Bearer 5029d5ae232e353d675936dac458f9f2'
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

            $response = curl_exec($ch);
            curl_close($ch);
            $this->r_data['response'] = $response;
            log_message('debug', 'Finviet UPDATE ' .$type. ' - DATA - ' . json_encode($data) . ' - RESPONSE - ' . $response);
        }
    }

    public function refind_rider($orderId) {
        $this->load->model('order_model');
        $order = $this->order_model->getById($orderId);
        if (!empty($order->id)) {
            if (!empty($order->rider_id)) {

            } else {

            }
        }
        return $this->r_data;
    }

    public function admin_cancel_order($orderId, $reason = "Hủy", $refind = false) {
        $this->load->model('user_model');
        $this->load->model('dbcommon');
        $this->load->model('order_model');
        $this->r_data['success'] = 0;
        $this->r_data['message'] = 'Order Not Found..!';

        $order = $this->order_model->getById($orderId);
        if ($order) {
            $noti_data = array(
                "userid" => $order->rider_id,
                "riderid" => $order->rider_id,
                "message" => "Admin vừa hủy đơn hàng " . $order->merchant_order_id,
                "order_id" => $orderId,
                "isbided" => '0',
                "type" => "cancelorder",
                "status" => "cancelorder",
                "createdate" => date('Y-m-d H:i:s', time()),
                "note" => $reason
            );
            $this->user_model->saveNotification($noti_data);

            //push notification
            $this->load->model('common_model');
            if (!empty($order->rider_id)) {
                $userresult = $this->user_model->getUser($order->rider_id);
                $noti_data['ordertype'] = $order->ordertype;

                $ntmsg = "Admin vừa hủy đơn hàng " . $order->merchant_order_id;
                $message = array("message" => $ntmsg, "data" => $noti_data, "mtype" => "cancelorder");
                //$messageios = $ntmsg;
                $msg = array('title' => 'Thông báo', 'body' => $ntmsg);
                $iosdeviceids = $and_ids = array();
                if (!empty($userresult)) {
                    if ($userresult->platform == 'ios' && !empty($userresult->deviceid)) {
                        $iosdeviceids[] = $userresult->deviceid;
                    } else if ($userresult->platform == 'android' && !empty($userresult->deviceid)) {
                        $and_ids[] = $userresult->deviceid;
                    }
                }

                if (!empty($and_ids)) {
                    $this->common_model->sendPushNotification($and_ids, $message, $msg, 'android');
                    $this->r_data['notification'] = 1;
                }
                if (!empty($iosdeviceids)) {
                    $this->common_model->sendPushNotification($iosdeviceids, $message, $msg, 'ios');
                    $this->r_data['notification'] = 1;
                }
            }
            $data = array('status' => 'Canceled', 'riderstatus' => 'Complete', "admincancel" => $reason, "cancel_reason" => $reason, "cancel_time" => date('Y-m-d H:i:s', time()));
            $orderupdate = $this->order_model->updateOrderById($data, $orderId);
            if ($orderupdate) {
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'You have canceled order #' . $orderId;
                if ($order->platform == 'api') {
                    $finvietData = [
                        'orderId' => $order->merchant_order_id,
                        'transactionId' => $order->transaction_id,
                        'riderId' => $order->rider_id ? $order->rider_id : 0,
                        'riderName' => $order->rider_id ? $userresult->firstname : "",
                        'riderContact' => $order->rider_id ? $userresult->contactno : "",
                        'riderWallet' => $order->rider_id ? $userresult->contactno : "",
                        'riderAvatar' => $order->rider_id ? ($userresult->profilepic ? $userresult->profilepic : no_image_default) : "",
                        'riderLatitude' => $order->rider_id ? $userresult->lat : 0,
                        'riderLongitude' => $order->rider_id ? $userresult->lng : 0,
                        'status' => 5,
                        'cancelReason' => $reason
                    ];
                    if ($refind) {
                        $finvietData['statusCode'] = 'FAILED';
                        $data = array(
                            'status' => 'Pending',
                            'riderstatus' => 'Pending',
                            'rider_id' => 0,
                            'distance' => 0,
                            'current_round' => 0
                        );
                        $result = $this->order_model->updateOrderById($data, $orderId);
                        if ($result) {
                            $this->load->library('redis_client');
                            $token = $this->user_model->getUserToken($order->userid);
                            $notificationData = [
                                "token" => $token->token,
                                "user_id" => $order->userid,
                                "order_id" => $orderId,
                                "lat" => $order->plat,
                                "lng" => $order->plng,
                                "radius" => 5
                            ];
                            $this->redis_client->sendNewOrderNotification($notificationData);
                        }
                    } else {
                        $finvietData['statusCode'] = 'CANCELED';
                    }
                    $this->finviet_notification('deliveryStatus', $finvietData);
                }
            } else {
                $this->r_data['success'] = 1;
                $this->r_data['message'] = 'Order Already Canceled';
            }
        }
        return $this->r_data;
    }

    /*
     * get counter for unread notification
     */

    public function get_unread_counter($userid, $usertype)
    {
        $this->load->model('user_model');
        $this->load->model('dbcommon');
        $user_data = $this->user_model->getUser($userid);
        if ($user_data) {
            if ($usertype == 'driver') {
                $where = "where `status` IN('assign_by_user','reject_by_user','complete_by_user','neworder','make_payment','change_accepted','give_tips','payment_at_des','cancelorder','admin') AND riderid = '" . $userid . "' AND isview = '0'";
            }

            if ($usertype == 'user') {
                $where = "where `status` IN('pickup','dropoff','at_destination','bid','complete_by_rider','accept_payment','change_paid','reached_destination','at_dropoff','payment_at_des','cancelorder','arrivepickup','admin','nobid') AND userid = '" . $userid . "' AND isview = '0'";
            }

            $counter = $this->dbcommon->getnumofdetails($this->common->getNotificationTable(), $where);
            if ($counter > 0) {
                return $counter;
            } else {
                return "0";
            }
        } else {
            return "0";
        }
    }
}
