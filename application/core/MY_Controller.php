<?php
/**
 * Created by PhpStorm.
 * User: nauticlash
 * Date: 8/30/19
 * Time: 13:39
 */

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->r_data['message'] = $this->r_data['data'] = '';
        $this->r_data['success'] = 0;
    }

    public function paramValidation($arrayParams, $data)
    {
        $noValueParam = '';
        foreach ($arrayParams as $val) {
            if ($data[$val] == '') {
                $noValueParam[] = $val;
            }
        }
        if (is_array($noValueParam) && count($noValueParam) > 0) {
            $this->r_data['message'] = 'Sorry, that is not valid input. You missed ' . implode(', ', $noValueParam) . ' parameters';
        } else {
            $this->r_data['success'] = 1;
        }
        return $this->r_data;
    }

    function returnData() {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($this->r_data));
    }
}