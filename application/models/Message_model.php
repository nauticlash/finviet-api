<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Message_model extends CI_Model
{
    private $table = "message";

    public function getUserConversation($fromId = 1, $toId = 1) {
        $this->db->select('fromId, toId, message, isViewed, create_date, created_at');
        $this->db->where("(fromId = $fromId AND toId = $toId) OR (fromId = $toId AND toId = $fromId)");
//        $this->db->where("fromId = $toId OR toId = $toId");
        $this->db->order_by('id', 'DESC');
        return $this->db->get($this->table)->result();
    }

    public function insertMessage($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
}
