<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Order_model extends CI_Model
{
    private $table = "order_master";

    public function getById($id) {
        $this->db->where('id', $id);
        return $this->db->get($this->table)->row();
    }

    public function getRiderPendingOrders($userId) {
        $new_time = date("Y-m-d H:i:s", strtotime('-12 hours'));
        $this->db->select("o.id, o.id as orderid, o.userid, o.rider_id, o.rider_id as isbided, user_cash_payment, user_cash_payment as fare_amount, pickup_address, plat, plng, delivery_address, dllat, dllng, description, sender_name, recipient_name, o.status, riderstatus, no_of_km, pickup_time, is_pickup, dropoff_time, is_dropoff, orderdate, delivery_time, order_items, extra, merchant_order_id, merchant_contact, order_total, customer_contact, cancel_time, rider_accept_time, cancel_reason, o.created_at, (select count(rider_id) from order_master WHERE rider_id='" . $userId . "' AND  riderstatus='Pending') as is_assigned, shipping_amount, pickup_amount, dropoff_amount");

        $this->db->where('is_deleted', '0');
        $this->db->where('o.status', 'Pending');
        $this->db->where('rider_id', 0);
        $this->db->where('orderdate >=', $new_time);
        $this->db->join($this->common->getNotificationTable() . ' AS n', 'n.order_id = o.id');
        $this->db->where('n.riderid', $userId);
        $this->db->where('n.type', 'neworder');
        $this->db->where('n.isbided', 0);

        $this->db->order_by('o.id', 'DESC');
        $data = $this->db->get($this->table . ' as o')->result();
        return $data;
    }

    public function getRiderAssignedOrders($userId) {
        $this->db->select("id, id as orderid, userid, rider_id, user_cash_payment, user_cash_payment as fare_amount, pickup_address, plat, plng, delivery_address, dllat, dllng, description, sender_name, recipient_name, status, riderstatus, no_of_km, pickup_time, is_pickup, dropoff_time, is_dropoff, orderdate, delivery_time, order_items, extra, merchant_order_id, merchant_contact, order_total, customer_contact, cancel_time, rider_accept_time, cancel_reason, created_at, shipping_amount, pickup_amount, dropoff_amount");

        $this->db->where('is_deleted', '0');
        $this->db->where('status', 'In Progress');
        $this->db->where('rider_id', $userId);

        $this->db->order_by('id', 'DESC');
        $data = $this->db->get($this->table)->result();
        return $data;
    }

    public function getRiderCompletedOrders($userId) {
        $this->db->select("id, id as orderid, userid, rider_id, user_cash_payment, user_cash_payment as fare_amount, pickup_address, plat, plng, delivery_address, dllat, dllng, description, sender_name, recipient_name, status, riderstatus, no_of_km, pickup_time, is_pickup, dropoff_time, is_dropoff, orderdate, delivery_time, order_items, extra, merchant_order_id, merchant_contact, order_total, customer_contact, cancel_time, rider_accept_time, cancel_reason, created_at, shipping_amount, pickup_amount, dropoff_amount");

        $this->db->where('is_deleted', '0');
        $this->db->where('riderstatus', 'Complete');
        $this->db->where('rider_id', $userId);

        $this->db->order_by('rider_accept_time', 'DESC');
        $data = $this->db->get($this->table)->result();
        return $data;
    }

    public function getRiderIsAssigned($userId) {
        $this->db->select("count(rider_id) as is_assigned");

        $this->db->where('rider_id', $userId);
        $this->db->where('is_deleted', '0');
        $this->db->where('status', 'In Progress');

        $this->db->group_by('rider_id');

        return $this->db->get($this->table)->row();
    }

    public function getOrderDetails($id) {
        $this->db->select("id, id as orderid, userid, rider_id, user_cash_payment, user_cash_payment as fare_amount, pickup_address, plat, plng, delivery_address, dllat, dllng, description, sender_name, recipient_name, status, riderstatus, no_of_km, pickup_time, is_pickup, dropoff_time, is_dropoff, orderdate, delivery_time, order_items, extra, merchant_order_id, merchant_contact, order_total, customer_contact, cancel_time, rider_accept_time, cancel_reason, created_at, need_image_verify, image_verify_type, shipping_amount, pickup_amount, dropoff_amount");
        $this->db->where('id', $id);
        return $this->db->get($this->table)->row();
    }


    public function updateOrderById($data, $id)
    {
        $redis_enable = config_item('redis_enable');
        if ($redis_enable === true) {
            $this->load->library('redis_client');
            $this->redis_client->setChangedOrderDetail($id);
        }
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function updateOrder($data, $where)
    {
        $flag = false;
        if (!empty($where['id'])) {
            $redis_enable = config_item('redis_enable');
            if ($redis_enable === true) {
                $this->load->library('redis_client');
                $this->redis_client->setChangedOrderDetail($where['id']);
            }
            $this->db->where('id', $where['id']);
            $flag = true;
        }
        if (isset($where['rider_id'])) {
            $this->db->where('rider_id', $where['rider_id']);
            $flag = true;
        }
        if ($flag) {
            $this->db->update($this->table, $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function updateOrderShippingFee($id)
    {
        $redis_enable = config_item('redis_enable');
        if ($redis_enable === true) {
            $this->load->library('redis_client');
            $this->redis_client->setChangedOrderDetail($id);
        }
        $this->db->set('user_cash_payment', 'user_cash_payment', false);
        $this->db->where('id', $id);
        $this->db->update($this->table);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }
}
