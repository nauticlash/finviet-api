<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

    /**
     * user login
     */
    public function userLogin($username, $password, $usertype)
    {
        $this->db->select("*, id as userid");
        $this->db->where("(contactno='$username' OR email='$username')");
        $this->db->where('password', md5($password));
        $this->db->where("usertype", $usertype);
        $data = $this->db->get($this->common->getUserTable())->row();
        if ($data) {
            if (!empty($data->profilepic)) {
                $data->profilepic = user_images . $data->profilepic;
            } else {
                $data->profilepic = no_image_default;
            }
        }
        return $data;
    }

    /**
     * get user by token
     */
    public function getUserbyToken($token)
    {
        $this->gdb->where('emailverify', $token);
        $data = $this->gdb->get($this->common->getUserTable())->row();
        return $data;
    }

    /**
     * Add Order
     */
    public function addOrder($data)
    {
        $this->db->insert($this->common->getOrderTable(), $data);
        return $this->db->insert_id();
    }

    /**
     * Add user
     */
    public function addUser($data)
    {
        $this->db->insert($this->common->getUserTable(), $data);
        return $this->db->insert_id();
    }

    /**
     * Update user
     */
    public function updateUser($data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update($this->common->getUserTable(), $data);
    }

    public function updateLocation($userid, $clat, $clng)
    {
        $redis_enable = config_item('redis_enable');
        if ($redis_enable === true) {
//            log_message('debug', "updateLocation: redis_enabled");
            $this->load->library('redis_client');
            $this->redis_client->updateLocation($userid, $clat, $clng);
        }
        $update_time = date('Y-m-d H:i:s', time());
        return $this->updateUser(array('lat' => $clat, 'lng' => $clng, 'modify_date' => $update_time, 'last_location_updated' => $update_time), $userid);

    }

    private function getRiderPositionFromCache($riderId)
    {
        $redis_enable = config_item('redis_enable');
        if ($redis_enable === true) {
            $this->load->library('redis_client');
            return $this->redis_client->getLocation($riderId);
        }
        return false;

    }

    public function updateDeviceUser($data, $id)
    {
        $this->db->where('deviceid', $id);
        return $this->db->update($this->common->getUserTable(), $data);
    }

    /**
     * get user
     */
    public function deleteUser($id)
    {
        $this->load->library('redis_client');
        $this->redis_client->removeUserTracking($id);
        $this->db->where('id', $id);
        return $this->db->delete($this->common->getUserTable());
    }

    /**
     * get All user
     */
    public function getAllUser()
    {
        $this->db->where('status', 0);
        $data = $this->db->get($this->common->getUserTable())->result();

        foreach ($data as $key => $value) {
            $data[$key] = $value;
            $data[$key]->profilepic = base_url('assets/images/default.png');
            if ($value->user_image) {
                $data[$key]->profilepic = user_images . $value->user_image;
            }

        }

        return $data;
    }
    /**
     * getOnlineRiders
     */
    public function getOnlineRiders()
    {
        $this->db->where('online_status', 'online')->where('usertype', 'driver');
        $data = $this->db->get($this->common->getUserTable())->result();
        return $data;
    }
    /**
     * get Driver Order History
     */
    public function getDriverOrderHistory($driverid)
    {
        $this->db->where('driverid', $driverid);
        $data = $this->db->get($this->common->getOrderTable())->result();
        return $data;
    }

    /**
     * get Driver Order In Process
     */
    public function getDriverOrderInprocess($driverid)
    {
        $this->db->where('driverid', $driverid);
        $this->db->where('status', 'ongoing');
        $data = $this->db->get($this->common->getOrderTable())->result();
        return $data;
    }

    /**
     * get Driver Order Assigned
     */
    public function getDriverAssignOrder($driverid)
    {
        // $this->db->select("s.*,u.firstname,u.lastname,u.email,u.contactno,(select count(rider_id) from order_master WHERE rider_id='".$driverid."' AND riderstatus!='Complete') as is_assigned");

        $this->db->select("s.images, s.promoamount, s.user_cash_payment, s.orderid, s.userid, s.rider_id, s.ordertype, s.cash_payment, s.tips, s.pickup_address, s.destination_address, s.delivery_address, s.delivery_type, s.description, s.status, s.riderstatus, s.no_of_km, s.orderdate, s.is_pickup,u.firstname,u.lastname,u.email,u.contactno,(select count(rider_id) from order_master WHERE rider_id='" . $driverid . "' AND riderstatus!='Complete') as is_assigned, s.order_total, s.order_items, s.merchant_order_id, s.plat, s.plng, s.dllat, s.dllng");

        $this->db->where('s.rider_id', $driverid);
        $this->db->where('s.status', 'In Progress');
        $this->db->where_in('s.riderstatus', array('Pending', 'PickupDone'));
        $this->db->where('s.is_deleted', '0');
        $this->db->where('s.rider_deleted', '0');
        $this->db->order_by('s.orderid', 'DESC');
        $this->db->join($this->common->getUserTable() . ' AS u', 'u.userid = s.userid', 'LEFT');
        $data = $this->db->get($this->common->getOrderTable() . ' AS s')->result();
        return $data;
    }

    /**
     * get Order History
     */
    public function getOrderHistory($userid)
    {
        //$status = array('delivered','complete');
        //$this->db->where('userid', $userid);
        //$this->db->where_in('status', $status);
        //$this->db->order_by('status', 'ASC');
        //$this->db->order_by('orderid', 'DESC');
        $query = "SELECT * FROM `order_master` WHERE `userid` = '" . $userid . "' AND (is_deleted='0' AND user_deleted='0')  ORDER BY CASE `status` WHEN 'In Progress' THEN 1 WHEN 'Pending' THEN 2 WHEN 'Complete' THEN 3 WHEN 'Canceled' THEN 4 END ,orderid DESC";
        $data = $this->db->query($query)->result();
        // echo $this->db->last_query();
        return $data;
    }

   
    /**
     * get Order Details
     */
    public function getOrderById($orderid, $type, $userid)
    {
        //$this->db->where('s.is_deleted', 0);
        $this->db->where('s.orderid', $orderid);
        $this->db->where('s.is_deleted', 0);
        if ($type == "user") {
            $this->db->select("s.*,IF(s.receipt='','',concat('" . user_images . "',s.receipt)) as receipt,u.firstname,u.lastname,u.email,u.contactno,u.lat,u.lng,IF(u.profilepic='','',concat('" . user_images . "',u.profilepic)) as profilepic,IF(s.signature_image='','',concat('" . user_images . "',s.signature_image)) as signature_image");
            $this->db->join($this->common->getUserTable() . ' AS u', 'u.userid = s.rider_id', 'LEFT');
        } else if ($type == "driver") {
            $this->db->select("s.*,IF(s.receipt='','',concat('" . user_images . "',s.receipt)) as receipt,u.firstname,u.lastname,u.email,u.contactno,,IF(u.profilepic='','',concat('" . user_images . "',u.profilepic)) as profilepic,IF(s.signature_image='','',concat('" . user_images . "',s.signature_image)) as signature_image,(select count(id) from notification_master WHERE order_id='" . $orderid . "' AND riderid='" . $userid . "' AND type='bid') as isbided,(select count(rider_id) from order_master WHERE rider_id='" . $userid . "' AND  riderstatus='Pending' ) as is_assigned");
            $this->db->join($this->common->getUserTable() . ' AS u', 'u.userid = s.userid', 'LEFT');
        }

        $data = $this->db->get($this->common->getOrderTable() . ' AS s')->row();
        // echo $this->db->last_query();
        if (!empty($data)) {
            if (empty($data->firstname)) {
                $data->firstname = "";
            }
            if (empty($data->lastname)) {
                $data->lastname = "";
            }
            if (empty($data->email)) {
                $data->email = "";
            }
            if (empty($data->contactno)) {
                $data->contactno = "";
            }
        }

        return $data;
    }

    /*
     * update notification by notification id
     */

    public function updateNoti($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($this->common->getNotificationTable(), $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    /*
     * update notification by notification id
     */

    public function deleteNotification($id)
    {
        if (is_array($id)) {
            $this->db->where_in('id', $id);
        } else {
            $this->db->where('id', $id);
        }
        return $this->db->delete($this->common->getNotificationTable());

    }

    public function updatetabnoti($userId, $usertype, $data, $id = 0)
    {
        if (!empty($id)) {
            $this->db->where('id', $id);
        }
        if ($usertype == "user") {
            $this->db->where('userid', $userId);
        }
        if ($usertype == "driver") {
            $this->db->where('riderid', $userId);
        }
        $this->db->update($this->common->getNotificationTable(), $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function insertWalletTransaction($data)
    {
        return $this->db->insert("wallet_transaction", $data);
    }

    /*
     * update Order by  Chat
     */

    public function RemoveChat($userid, $riderid, $data)
    {
        $this->db->where(array('fromId' => $userid, 'toId' => $riderid));
        $this->db->or_where(array('fromId' => $riderid, 'toId' => $userid));
        $this->db->update($this->common->getMessageTable(), $data);
        return true;
    }

    /**
     * get user
     */
    public function getUser($id)
    {
        $this->db->where('id', $id);
        $data = $this->db->get($this->common->getUserTable())->row();
        if (!empty($data)) {

            if (!empty($data->profilepic)) {
                $data->profilepic = user_images . $data->profilepic;
            } else {
                $data->profilepic = no_image_default;
            }
        }
        return $data;
    }

    /**
     * get user details with session
     */
    public function getUserWithSession($token_id, $type)
    {
        $this->db->where('token', $token_id);
        $this->db->where('login_type', $type);
        $this->db->join($this->common->getUserTable() . ' AS u', 'u.id = s.userid', 'LEFT');
        return $this->db->get($this->common->getUserSessionTable() . ' AS s')->row();
    }

    /**
     * check user exists or not
     */
    public function checkUserExists($type, $val, $usertype)
    {
        $this->db->where('usertype', $usertype);
        if ($type == 'email') {
            $this->db->where('email', $val);
        }

        if ($type == 'contactno') {
            $this->db->where('contactno', $val);
        }

        if ($type == 'email' || $type == 'contactno') {
            $fdata = $this->db->get($this->common->getUserTable())->row();
        }

        if (!empty($fdata)) {
            return true;
        } else {
            return false;
        }
    }

    public function getExistsUser($phone, $usertype) {
        $this->db->where('usertype', $usertype);
        $this->db->where('contactno', $phone);
        return $this->db->get($this->common->getUserTable())->row();
    }

    /**
     * check user exists or not
     */
    public function checkEmailExists($type, $val, $usertype)
    {
        $this->db->where('usertype', $usertype);
        if ($type == 'email') {
            $this->db->where('email', $val);
        }

        if ($type == 'email') {
            $fdata = $this->db->get($this->common->getUserTable())->row();
        }

        if (!empty($fdata)) {
            return $fdata;
        } else {
            return false;
        }
    }

    /**
     * check forgot user exists or not
     */
    public function checkForgotUser($value, $usertype)
    {
        $this->db->where("email", $value);
        $this->db->where("usertype", $usertype);
        return $this->db->get($this->common->getUserTable())->row();
        //echo  $this->db->last_query(); die;
    }

    /**
     * Update Order status
     */
    public function UpdateOrderStatus($orderid, $driverid, $orderstatus)
    {

        if ($orderstatus['status'] == "ongoing") {
            $query = "select o.*,u.userid,u.firstname,u.lastname,u.email,u.contactno,u.profilepic,platform,deviceid from order_master o JOIN user_master u ON o.userid=u.userid  where o.orderid='" . $orderid . "'";
            $query = $this->db->query($query);
            $orderinfo = $query->row();
            if (!empty($orderinfo->deviceid)) {

                $message = array("message" => "Your order has been pickuped successfully!");
                $registration_ids = array($orderinfo->deviceid);
                $this->sendPushNotification($registration_ids, $message);
                $notyarray = array("userid" => $orderinfo->userid, "message" => $message['message'], "status" => 'pickup', "createdate" => date('Y-m-d H:i:s', time()));
                $save = $this->db->insert($this->common->getNotificationTable(), $notyarray);
            }
        } else if ($orderstatus['status'] == "delivered") {
            $query = "select o.*,u.userid,u.firstname,u.lastname,u.email,u.contactno,u.profilepic,platform,deviceid from order_master o JOIN user_master u ON o.userid=u.userid  where o.orderid='" . $orderid . "'";
            $query = $this->db->query($query);
            $orderinfo = $query->row();
            if (!empty($orderinfo->deviceid)) {

                $message = array("message" => "Your order has been delivered successfully!");
                $registration_ids = array($orderinfo->deviceid);
                $this->sendPushNotification($registration_ids, $message);
                $notyarray = array("userid" => $orderinfo->userid, "message" => $message['message'], "status" => 'delivered', "createdate" => date('Y-m-d H:i:s', time()));
                $save = $this->db->insert($this->common->getNotificationTable(), $notyarray);
            }
        } else {
            return true;
        }
        // $this->db->where('orderid', $orderid);
        // $this->db->where('driverid', $driverid);
        // return $this->db->update($this->common->getOrderTable(), $orderstatus);
        return $this->updateorderbyid($orderid,$orderstatus);
    }

    /**
     * get Order status
     */
    public function getOrderStatus($orderid, $driverid)
    {
        $this->db->where('orderid', $orderid);
        $this->db->where('driverid', $driverid);
        return $this->db->get($this->common->getOrderTable())->row();
    }

    /**
     * get User Order
     */
    public function getuserOrder($userid)
    {
        $this->db->select('count(id) as ordercount');
        $this->db->where('userid', $userid);
        return $this->db->get($this->common->getOrderTable())->row()->ordercount;
    }

    /*

    /**
     * get Order status
     */

    public function checkOrderStatus($orderid)
    {
        $this->db->where('orderid', $orderid);
        $this->db->where('status', 'Pending');
        $this->db->where('riderstatus', 'Pending');
        $this->db->where('rider_id', 0);
        $this->db->where('cancelby', 0);
        $this->db->where('user_deleted', 0);
        $this->db->where('is_deleted', 0);
        return $this->db->get($this->common->getOrderTable())->row();
    }

    /**
     * get User Requested Order
     */
    public function getUserOrderDashboard($userid)
    {

        $status = array('pending', 'pickup', 'ongoing');
        $this->db->select('s.*,u.firstname,u.lastname,u.email,u.contactno');
        $this->db->where('s.userid', $userid);
        $this->db->where_in('s.status', $status);
        $this->db->join($this->common->getUserTable() . ' AS u', 'u.userid = s.rider_id', 'LEFT');
        // $this->db->join($this->common->getUserTable() . ' AS u', 'u.userid = s.driverid', 'LEFT');
        $data = $this->db->get($this->common->getOrderTable() . ' AS s')->result();
        //echo $this->db->last_query();
        return $data;
    }

    /**
     * get Notification
     */
    public function getNotification($id, $type, $page = 0)
    {
        $subarray = '';
        $count = 0;
        $limit = 30;
        $from = $page * $limit;
        $to = $from + $limit;
        if ($type == 'user') {

            $subarray = "(select count(id) as ncount from notification_master where `status` IN('pickup','dropoff','at_destination','bid','complete_by_rider','accept_payment','change_paid','reached_destination','at_dropoff','payment_at_des','cancelorder','admin','arrivepickup','nobid')  AND userid='" . $id . "' and isview='0' )";
            $subcount = $this->db->query($subarray)->row();
            $count = $subcount->ncount;

            //$query = "SELECT m.* FROM `notification_master` m WHERE m.id IN (SELECT MAX(s.id) FROM notification_master s WHERE  s.`userid` = '" . $id . "' GROUP BY order_id ORDER BY m.createdate DESC) AND  m.status IN('pickup','dropoff','at_destination','bid','complete_by_rider','accept_payment','change_paid','reached_destination','at_dropoff','payment_at_des','cancelorder','admin','arrivepickup','nobid') GROUP BY m.order_id ORDER BY m.id DESC LIMIT 10";
            $query = "SELECT notification_master.* FROM notification_master WHERE notification_master.id IN (SELECT MAX(s.id) as ids FROM notification_master s WHERE s.`userid` = '" . $id . "' AND s.status IN ('pickup','dropoff','at_destination','bid','complete_by_rider','accept_payment','change_paid','reached_destination','at_dropoff','payment_at_des','cancelorder','admin','arrivepickup','nobid') GROUP BY s.order_id ORDER BY s.createdate DESC) ORDER BY notification_master.id DESC LIMIT 10";

        } else if ($type == 'driver') {
            $subarray = "(select count(id) as ncount from notification_master where  `status` IN('assign_by_user','reject_by_user','complete_by_user','neworder','make_payment','change_accepted','give_tips','payment_at_des','cancelorder','admin','complete_by_rider') AND riderid='" . $id . "' and isview='0' ) ";
            $subcount = $this->db->query($subarray)->row();
            $count = $subcount->ncount;

            $query = "SELECT notification_master.* FROM notification_master WHERE notification_master.id IN (SELECT s.id as ids FROM notification_master s WHERE s.`riderid` = '" . $id . "' AND s.status IN ('assign_by_user','reject_by_user','complete_by_user','neworder','make_payment','change_accepted','give_tips','payment_at_des','cancelorder','admin','complete_by_rider') ORDER BY s.createdate DESC) ORDER BY notification_master.id DESC LIMIT $from, $to";

            //$query = "SELECT m.* FROM `notification_master` m WHERE m.id IN (SELECT MAX(s.id) FROM notification_master s WHERE  s.`riderid` = '" . $id . "' AND  status IN('assign_by_user','reject_by_user','complete_by_user','neworder','make_payment','change_accepted','give_tips','payment_at_des','cancelorder','admin') GROUP BY order_id ORDER BY m.createdate DESC) ORDER BY m.id DESC LIMIT 10";
        }

        $data['data'] = $this->db->query($query)->result();
        $data['count'] = $count;
        return $data;
    }

    /**
     * Save Notification
     */
    public function saveNotification($data)
    {
        $data = $this->db->insert($this->common->getNotificationTable(), $data);
        return $data;
    }

    /**
     * Post Bid Update Notification
     */
    public function PostbidStatus($orderid, $riderid, $data)
    {
        $this->db->where('userid', $riderid);
        $this->db->where('order_id', $orderid);
        $this->db->where('type', 'neworder');
        $this->db->update($this->common->getNotificationTable(), $data);
        return $this->db->affected_rows() > 0;
    }

    /**
     *  Push Notification
     */
    public function sendPushNotification($registration_ids, $message, $msg = array('title' => 'Bungkusit', 'body' => ""), $platform = '')
    {

        $url = 'https://fcm.googleapis.com/fcm/send';
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . fcm_key,
        );
        $data = array(
            'registration_ids' => $registration_ids,
        );
        if ($platform == 'android') {
            $data['data'] = $message;
        }
        if ($platform == 'ios') {
            $data['notification'] = $msg;
            $data['notification']['sound'] = "default";
            $data['data'] = $message;
        }
        //echo json_encode($data); die;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        if ($headers) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
        //print_r($response);
    }

    public function sendPushNotificationios($deviceToken1 = "", $message1 = "", $data = "", $mtype = "")
    {

        $deviceToken = $deviceToken1;
        //// Put your private key's passphrase here:
        $passphrase = '123';
        //
        // Put your alert message here:
        //$message = 'food menu has been changed!';
        $message = $message1;

        ////////////////////////////////////////////////////////////////////////////////

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', '../Certificates.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client(
            'ssl://gateway.sandbox.push.apple.com:2195', $err,
            $errstr, 25, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp) {
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        }

        // echo PHP_EOL;
        // echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'data' => $data,
            'mtype' => $mtype,
        );

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
        //echo $result;
        if (!$result) {
            echo 'Message not delivered' . PHP_EOL;

        } else {
            //echo 'Message successfully delivered ... Good' . PHP_EOL;
        }
        // Close the connection to the server
        fclose($fp);
    }

    /**
     * check Promocode
     */
    public function checkPromocode($promocode)
    {
        $today = date('Y-m-d');
        $this->db->where("start_datetime <= '$today' and end_datetime >= '$today' and is_deleted = '0'");
        $this->db->where('promocode', $promocode);
        return $this->db->get($this->common->getPromoTable())->row();
    }

    /**
     * check Promocode
     */
    public function Ckeckbided($orderid, $riderid, $userid)
    {

        $this->db->where("order_id='$orderid' AND riderid = '$riderid' AND userid = '$userid' AND isbided='1'");

        return $this->db->get($this->common->getNotificationTable())->row();
        //echo $this->db->last_query();
    }

    /**
     * get fb user
     */
    public function getFbUser($fbid, $usertype)
    {
        $this->db->where("fbid", $fbid);
        $this->db->where("usertype", $usertype);
        $data = $this->db->get($this->common->getUserTable())->row();

        //echo $this->db->last_query(); die;

        if ($data) {

            if (!empty($data->profilepic)) {
                $data->profilepic = user_images . $data->profilepic;
            } else {
                $data->profilepic = no_image_default;
            }

            if (!empty($data->front_photo)) {
                $data->front_photo = user_images . $data->front_photo;
            } else {
                $data->front_photo = no_image_default;
            }

            if (!empty($data->back_photo)) {
                $data->back_photo = user_images . $data->back_photo;
            } else {
                $data->back_photo = no_image_default;
            }

            if (!empty($data->ic_front_photo)) {
                $data->ic_front_photo = user_images . $data->ic_front_photo;
            } else {
                $data->ic_front_photo = no_image_default;
            }

            if (!empty($data->ic_back_photo)) {
                $data->ic_back_photo = user_images . $data->ic_back_photo;
            } else {
                $data->ic_back_photo = no_image_default;
            }

            if (!empty($data->bike_number_plate)) {
                $data->bike_number_plate = user_images . $data->bike_number_plate;
            } else {
                $data->bike_number_plate = no_image_default;
            }

            if (!empty($data->bike_pic)) {
                $data->bike_pic = user_images . $data->bike_pic;
            } else {
                $data->bike_pic = no_image_default;
            }

            if (!empty($data->insurance_latter_pic)) {
                $data->insurance_latter_pic = user_images . $data->insurance_latter_pic;
            } else {
                $data->insurance_latter_pic = no_image_default;
            }

            if (!empty($data->road_tex_pic)) {
                $data->road_tex_pic = user_images . $data->road_tex_pic;
            } else {
                $data->road_tex_pic = no_image_default;
            }
        }
        return $data;
    }

    public function getFarePrice()
    {
        //$today = date('Y-m-d');
        //$this->db->where('is_deleted=', '0');
        //$this->db->where('holiday_date=', $today);
        //$dataholiday = $this->db->get('holiday_pricing')->row();
        //if (empty($dataholiday)) {
        $this->db->where('is_regular=', '1');
        $dataholiday = $this->db->get('holiday_pricing')->row();
        //}
        return $dataholiday;
    }

    public function getRiderwithorder($riderid, $orderid)
    {
        $this->db->select('user_master.userid,user_master.firstname,user_master.lastname,user_master.lat,user_master.lng,user_master.profilepic,order_master.orderid,order_master.plat,order_master.plng,order_master.pickup_address,
    (SELECT COUNT(orderid) FROM order_master WHERE rider_id = ' . $riderid . ' AND status = "Complete") AS ordertaken');
        $this->db->where('user_master.userid', $riderid);
        $this->db->where('order_master.orderid', $orderid);
        $this->db->join('notification_master', 'notification_master.riderid = user_master.userid', 'LEFT');
        $this->db->join('order_master', 'order_master.orderid = notification_master.order_id', 'LEFT');
        $data = $this->db->get('user_master')->row();
        if ($data) {
            return $data;
        } else {
            return false;
        }

    }

    /**
     * get Notification  data for rider assignment
     */
    public function getbiderdatabyorderid($userid, $riderid, $orderid)
    {
        $this->db->where('riderid', $riderid);
        $this->db->where('order_id', $orderid);
        $this->db->where('userid', $userid);
        $this->db->where('type', 'bid');
        $this->db->where('isbided', 1);
        $data = $this->db->get($this->common->getNotificationTable())->row();
        return $data;
    }

    /**
     * get Notification
     */
    public function getriderlistfrombid($id)
    {
        $this->db->select('user_master.userid as riderid,user_master.firstname,user_master.lastname,user_master.profilepic');
        $this->db->where('notification_master.order_id', $id);
        $this->db->where('notification_master.isbided', 1);
        $this->db->join('user_master', 'user_master.userid = notification_master.riderid', 'LEFT');
        $data = $this->db->get($this->common->getNotificationTable())->result();
        return $data;
    }

    /*
     * get bidderlist by order id
     */

    public function get_bidderlist_by_orderid($orderid)
    {
        $this->db->select("n.riderid,u.firstname,u.lastname,IF(u.profilepic='', '',concat('" . user_images . "', u.profilepic)) as profilepic");
        $this->db->where('n.order_id', $orderid);
        $this->db->where('n.type', "bid");
        $this->db->where('n.status !=', "reject_by_user");
        $this->db->group_by('n.riderid');
        $this->db->join('user_master AS u', 'u.userid = n.riderid', 'LEFT');
        $data = $this->db->get($this->common->getNotificationTable() . ' AS n')->result();
        //print_r($this->db->last_query()); die;
        return $data;
    }

    /**
     * get Current location of rider
     */
    public function getRidercurrentlocation($id)
    {
        $this->db->select('u.userid as riderid,u.lat,u.lng');
        $this->db->where('userid', $id);
        $data = $this->db->get($this->common->getUserTable() . ' AS u')->row();
        return $data;
    }

    /**
     * get message count
     */
    public function getUnreadMessagecounts($fromid)
    {
        $query = "SELECT count(id) as totalunread  FROM `message` WHERE toId = '" . $fromid . "' and isViewed ='0'";
        return $data = $this->db->query($query)->row()->totalunread;
    }


    public function addComplain($data)
    {
        $this->db->insert($this->common->getComplainTable(), $data);
        return $this->db->insert_id();
    }

    public function addfeedback($data)
    {
        $this->db->insert($this->common->getfeedbackTable(), $data);
        return $this->db->insert_id();
    }

    /*
     * get review of rider for specific order
     */

    public function getRiderReviewForOrder($orderid)
    {
        $this->db->select('(SUM(satisfaction) + SUM(communication) + SUM(ontime)) / 3 AS rates');
        $this->db->where('orderid', $orderid);
        return $this->db->get($this->common->getridersfeedbackTable())->row();
    }

    /*
     * get review of user for specific order
     */

    public function getUserReviewForOrder($orderid)
    {
        $this->db->select('(SUM(overallexperience) + SUM(communication)) / 2 AS rates');
        $this->db->where('orderid', $orderid);
        return $this->db->get($this->common->getusersfeedbackTable())->row();
    }

    /*
     * get review of rider
     */

    public function getRiderReview($riderid)
    {
        $this->db->select('(SUM(satisfaction) + SUM(communication) + SUM(ontime)) / (COUNT(feedback_id) * 3) AS rates ');
        $this->db->where('riderid', $riderid);
        return $this->db->get($this->common->getridersfeedbackTable())->row();
    }

    /*
     * get review of user
     */

    public function getUserReview($userid)
    {
        $this->db->select('(SUM(overallexperience) + SUM(communication)) / (COUNT(feedback_id) * 2) AS rates ');
        $this->db->where('userid', $userid);
        return $this->db->get($this->common->getusersfeedbackTable())->row();
    }

    /*
     * get indivisual review of rider
     */

    public function getIndivisualRatesOFRider($riderid, $status)
    {
        $this->db->select('SUM(' . $status . ') / COUNT(feedback_id) AS rates');
        $this->db->where('riderid', $riderid);
        return $this->db->get($this->common->getridersfeedbackTable())->row();
    }

    /*
     * get indivisual review of user
     */

    public function getIndivisualRatesOFUser($userid, $status)
    {
        $this->db->select('SUM(' . $status . ') / COUNT(feedback_id) AS rates');
        $this->db->where('userid', $userid);
        return $this->db->get($this->common->getusersfeedbackTable())->row();
    }

    /**
     * Add Subscription
     */
    public function addSubscription($data)
    {
        $this->db->insert($this->common->getSubscriptionTable(), $data);
        return $this->db->insert_id();
    }

    /*
     * check subsciption plan
     */
    public function checkSubscription($riderid)
    {
        return $this->db->query("SELECT *,IF(recimage='','',concat('" . sub_images . "',recimage)) as recimage FROM sub_rider_master WHERE "
            . "sub_id IN (SELECT MAX(sub_id) FROM sub_rider_master Where riderid = " . $riderid . " AND "
            . "DATE_FORMAT(sub_end_date,'%Y-%m-%d') > DATE_FORMAT(NOW(),'%Y-%m-%d')) ORDER BY sub_id DESC")->row();

    }
    /*
     * get user by referral code
     */
    public function getUserByReferralcode($referralcode, $userid = 0)
    {
        $this->db->where('referralcode', $referralcode);
        return $this->db->get($this->common->getUserTable())->row();
    }

    /*
     * get data of setting table
     */

    public function getSettings()
    {
        $this->db->where('setting_id', 1);
        return $this->db->get($this->common->getSettingTable())->row();
    }

    /*
     * get invoice data
     */
    public function subscriptionInvoice($sub_id)
    {
        $this->db->select('sub_rider_master.*,user_master.firstname,user_master.lastname,
                        user_master.address,user_master.email,user_master.contactno,user_master.wallet ,user_master.carry_forward');
        $this->db->join('user_master', 'user_master.userid = sub_rider_master.riderid', 'LEFT');
        $this->db->where('sub_rider_master.sub_id', $sub_id);
        return $this->db->get($this->common->getSubscriptionTable())->row();
    }

    /*
     * get data of advertize table
     */
    public function getAdvertise()
    {
        $this->db->select("id,IF(banner='','',concat('http://167.99.66.73/images/banner/',banner)) as banner,description, location, lat, lng");
        $this->db->where('is_deleted', 0);
        return $this->db->get('adv_master')->result();
    }

    /**
     * Add Adv click
     */
    public function addAdvclick($data)
    {
        $this->db->insert('advclick', $data);
        return $this->db->insert_id();
    }

    public function getUserToken($userId) {
        $this->db->select('token')->where('userid', $userId)->where('is_active', 1)->order_by('start_date', 'DESC')->limit(1);
        return $this->db->get('user_session')->row();
    }
}
