<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require './vendor/autoload.php';
use \Firebase\JWT\JWT;

class Common_model extends CI_Model {

    
    private $user_master = 'user_master';
    private $user_session = 'user_session';
    private $order_master = 'order_master';
    private $notification_master = 'notification_master';
    private $promocode_master = 'promocode_master';
    private $complain_master = 'complain_master';
    private $message = 'message'; // chat
    private $feedback = 'feedback'; // chat
    private $riders_feedback = 'riders_feedback';
    private $users_feedback = 'users_feedback';
    private $sub_rider_master = 'sub_rider_master';
    private $setting_master = 'setting_master';
    
    /*
     * get setting table
     */
    public function getSettingTable()
    {
        return $this->setting_master;
    }
    /*
     *  get subscription table
     */
    public function getSubscriptionTable()
    {
        return $this->sub_rider_master;
    }

    public function getusersfeedbackTable() {
        return $this->users_feedback;
    }
    
    public function getridersfeedbackTable() {
        return $this->riders_feedback;
    }

    public function getMessageTable() {
        return $this->message;
    }  
    public function getpromoTable() {
        return $this->promocode_master;
    }    

    public function getUserTable() {
        return $this->user_master;
    }

	public function getOrderTable() {
        return $this->order_master;
    }


    public function getUserSessionTable() {
        return $this->user_session;
    }
	
    public function getNotificationTable() {
        return $this->notification_master;
    }
    public function getComplainTable() {
        return $this->complain_master;
    }
     public function getfeedbackTable() {
        return $this->feedback;
    }

    /**
     * return user by id
     */
    public function getUserById($id) {
        $this->gdb->where('userid', $id);
        return $this->gdb->get($this->user_master)->row();
    }

    /**
     * Get secure key
     */
    public function getSecureKey() {
        $string = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $stamp = time();
        $secure_key = $pre = $post = '';
        for ($p = 0; $p <= 10; $p++) {
            $pre .= substr($string, rand(0, strlen($string) - 1), 1);
        }

        for ($i = 0; $i < strlen($stamp); $i++) {
            $key = substr($string, substr($stamp, $i, 1), 1);
            $secure_key .= (rand(0, 1) == 0 ? $key : (rand(0, 1) == 1 ? strtoupper($key) : rand(0, 9)));
        }

        for ($p = 0; $p <= 10; $p++) {
            $post .= substr($string, rand(0, strlen($string) - 1), 1);
        }
        return $pre . '-' . $secure_key . $post;
    }

    /**
     * Add user session data
     */
    public function insertUserSession($data) {
        $updateData = array('is_active' => 0,);
        $this->db->where('userid', $data['userid']);
        $this->db->where('login_type', 'login');
        $this->db->update($this->user_session, $updateData);
        $this->db->insert($this->user_session, $data);
        return $this->db->insert_id();
    }

    public function getUserSession($userid, $token) {
        $this->db->where('is_active', 1);
        $this->db->where('userid', $userid);
        $this->db->where('token', $token);
        return $this->db->get($this->user_session)->row();
    }

    public function getSessionInfo($secret_log_id) {
        $this->db->where('is_active', 1);
        $this->db->where('session_id', $secret_log_id);
        return $this->db->get($this->user_session)->row();
    }

    public function logoutUser($secret_log_id) {
        $data = array('is_active' => 0, 'end_date' => DATETIME);
        $this->db->where('session_id', $secret_log_id);
        $this->db->update($this->user_session, $data);
    }

    public function generatePassword() {
        $post = '';
        $string = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        for ($p = 0; $p <= 7; $p++) {
            $post .= substr($string, rand(0, strlen($string) - 1), 1);
        }
        return $post;
    }

    /**
     * Array sorting
     */
    public function aasort(&$array, $key, $order) {
        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va->$key;
        }
        if ($order == 'ASC')
            asort($sorter);
        else
            arsort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        return $array = array_values($ret);
    }

    /**
     * Send Email
     * old setting
     *  $config['smtp_host'] = 'ssl://md-in-24.webhostbox.net';
        $config['mail_path'] = 'ssl://md-in-24.webhostbox.net';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '30';
        $config['smtp_user'] = 'test@cearsinfotech.in';
        $config['smtp_pass'] = 'Zb2R^Pt^*[1]';
     */
    function sendMail($toEmail, $subject, $mail_body, $fromEmail = '', $fromName = '', $ccEmails = '', $replyTo = '') {
        $this->load->library('email');
        if (!$fromEmail)
            $fromEmail = FROM_EMAIL;

        if (!$fromName)
            $fromName = PROJECT_NAME;

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://md-in-24.webhostbox.net';
        $config['mail_path'] = 'ssl://md-in-24.webhostbox.net';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '30';
        $config['smtp_user'] = 'test@cearsinfotech.in';
        $config['smtp_pass'] = 'Zb2R^Pt^*[1]';
        $config['mailtype'] = 'html';
        $this->email->initialize($config);

        $this->email->from($fromEmail, $fromName);
        $this->email->to($toEmail);
        $this->email->subject($subject);
        $this->email->message($mail_body);

        if ($replyTo != "")
            $this->email->reply_to($replyTo, '');

        if ($ccEmails != "")
            $this->email->cc($ccEmails);
       
       return $this->email->send();
        //echo $this->email->print_debugger();
    }
	
     function sendMail1($toEmail, $subject, $mail_body, $fromEmail = '', $fromName = '', $ccEmails = '', $replyTo = '') {
       

//        require  'sendgrid/vendor/autoload.php';
//        $from = new SendGrid\Email("Bungkusit", "info@bungkusit.com");
//        $subject =$subject;
//        $to = new SendGrid\Email("Bungkusit",$toEmail);
//        $content = new SendGrid\Content("text/html", $mail_body);
//        $mail = new SendGrid\Mail($from, $subject, $to, $content);
//
//       $apiKey = "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"; //getenv('SENDGRID_API_KEY');
//       $sg = new \SendGrid($apiKey);
//
//        $response = $sg->client->mail()->send()->post($mail);
//        //echo "<pre>";
//        return $response->statusCode();
//        //print_r($response->headers());
//        $response->body();
    }

    /**
     * Verify user account
     */
    public function verifyAccount($verify_token) {
        $this->gdb->where('verify_token', $verify_token);
        $this->gdb->where('verify_token !=', 1);
        $this->gdb->where('code', 0);
        return $this->gdb->get($this->user_master)->row();
    }

    /**
     * Verify forgot password
     */
    public function verifyForgot($verify_token) {
        $this->gdb->where('verify_token', $verify_token);
        $this->gdb->where('verify_token !=', 1);
        $this->gdb->where('code !=', 0);
        return $this->gdb->get($this->user_master)->row();
    }

    /**
     * Successfully verify user account
     */
    public function successVerifyAccount($verify_token) {
        $data['verify_token'] = 1;
        $this->gdb->where('verify_token', $verify_token);
        $this->gdb->where('verify_token !=', 1);
        return $this->gdb->update($this->user_master, $data);
    }

    /**
     * Successfully verify forgot password
     */
    public function successVerifyForgot($verify_token, $code, $password) {
        $data['verify_token'] = 1;
        $data['code'] = 0;
        $data['password'] = md5($password);
        $this->gdb->where('verify_token', $verify_token);
        $this->gdb->where('code', $code);
        ;
        $this->gdb->where('verify_token !=', 1);
        return $this->gdb->update($this->user_master, $data);
    }

    /**
     * check verify forgot password code
     */
    public function checkVerifyForgot($verify_token, $code) {
        $data['verify_token'] = 1;
        $this->gdb->where('verify_token', $verify_token);
        $this->gdb->where('code', $code);
        $this->gdb->where('verify_token !=', 1);
        return $this->gdb->get($this->user_master)->row();
    }

    function sendPushNotificationios($deviceToken, $message = "",$data="") {
 
                return true;
                $passphrase = '123';
                $production = false;
                $message = $message;
                $ctx = stream_context_create();
                stream_context_set_option($ctx, 'ssl', 'local_cert', 'DevPushNileClient.pem'); 
                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

                // Open a connection to the APNS server
                   if ($production) 
                    {
                        $gateway = 'gateway.push.apple.com:2195';
                    } 
                    else
                    { 
                        $gateway = 'gateway.sandbox.push.apple.com:2195';
                    }
                    $fp = stream_socket_client(
                    'ssl://'.$gateway, $err,
                    $errstr, 25, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); 
                
                if (!$fp)
                exit("Failed to connect: $err $errstr" . PHP_EOL);

                // echo PHP_EOL;
                // echo 'Connected to APNS' . PHP_EOL;

                // Create the payload body
                $body['aps'] = array(
                'alert' => $message,
                'sound' => 'default',
                'data' => $data
                );

                // Encode the payload as JSON
                foreach($deviceToken as $device) 
                {
                    $payload = json_encode($body);
                    $msg = chr(0) . pack('n', 32) . pack('H*',$device) . pack('n', strlen($payload)) . $payload;
                    // Send it to the server
                    $result = fwrite($fp, $msg, strlen($msg));
                }
                //echo $result;
                if (!$result)
                {
                        echo 'Message not delivered' . PHP_EOL;
                }
                else
                {
                    // echo 'Message successfully delivered ... Good' . PHP_EOL;
                }
                // Close the connection to the server
            fclose($fp);
    }
    

    /**
    *  Push Notification
    */
    public function sendPushNotification($registration_ids, $message,$msg=array('title' => 'Finviet', 'body' =>""),$platform='') {

        $url = 'https://fcm.googleapis.com/fcm/send';
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . fcm_key
        );
        $data = array(
            'registration_ids' => $registration_ids
            );
		if($platform == 'android'){
			$data['data'] = $message;
		}	
		if($platform == 'ios'){
			$data['notification'] = $msg;
			$data['notification']['sound'] = "default";
			$data['data'] = $message;
		}	
        //echo json_encode($data);
        log_message('info', 'Send notification for --- ' . json_encode($data));
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        if ($headers)

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function getToken($user)
    {
        $tokenData = [
            'id' => $user->id,
            'name' => $user->firstname,
            'user_type' => $user->usertype,
            'contact_no' => $user->contactno
        ];
        $key = "bungkusit";
        return JWT::encode($tokenData, $key, 'HS512', '', array('admin' => true));
    }
}
