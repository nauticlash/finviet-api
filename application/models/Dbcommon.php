<?php
class Dbcommon extends CI_Model
{
		
	function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }
	function insert($table_name,$data)
	{
		if($this->db->insert($table_name, $data))
		{
			return $this->db->insert_id();
		}
		else
		{
			return false;
		}
	}
	function update($table_name,$array,$data)
	{
		foreach($array as $key=>$value)
		{
			$this->db->where($key,$value);
		}
		if($this->db->update($table_name, $data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function delete($table_name,$array)
	{
		foreach($array as $key=>$value)
		{
			$this->db->where($key,$value);
		}
		$this->db->delete($table_name); 
		return true;	
	}
	function checkpermission($table='',$other_table='',$id='',$value='0')
	{
		$sql="select count(*) as cnt from $table where";
		foreach($other_table as $tablename)
		{
			$sql.=" 0<(select count(*) as c from $tablename where $id='".$value."') or";
		}
		$sql=trim($sql,' or');
		$query = $this->db->query($sql);
		$result=$query->row();
		return $result->cnt;
	}
	function getdetails($table_name,$query,$offset='0',$limit='1')
	{
		$sql="select * from $table_name ".$query." LIMIT $offset,$limit";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getalldetails($table_name,$query)
	{
		$sql="select * from $table_name ".$query;
		$query = $this->db->query($sql);
		return $query->result();
	}
	

	function getnumofdetails($table_name,$where)
	{
		$sql="select count(*) as cnt from $table_name ".$where;
		$query = $this->db->query($sql);
		$result=$query->row();
		//echo $this->db->last_query(); die;
		return $result->cnt;
	}
	function getnumofdetails_($query)
	{
		$sql="select ".$query."";
		$query = $this->db->query($sql);
		return $query->num_rows(); 
	}
	
	function getdetailsinfo($table_name,$array)
	{
		$sql="select * from $table_name where ";
		foreach($array as $key=>$value)
		{
			$sql.=" $key='$value' and";
		}
		$sql=trim($sql,' and');
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function getalldetailsinfo($table_name,$array)
	{
		$sql="select * from $table_name where ";
		foreach($array as $key=>$value)
		{
			$sql.=" $key='$value' and";
		}
		$sql=trim($sql,' and');
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getInfo($field_name='',$table_name,$array='',$return="",$limit='',$orderby='')
	{
		if($field_name=='')
		{
			$sql='select * ';
		}
		else
		{
			$str='';
			foreach($field_name as $row)
			{
				$str.=$row.',';
			}
			$str=rtrim($str,',');
			$sql="select ".$str;
		}
		$sql.= ' from ' .$table_name .' where 2>0 ';
		if($array!='')
		{
			foreach($array as $key=>$value)
			{
				$sql.="and $key='$value' ";
			}
			$sql=trim($sql,' and');
		}
		if($orderby!='')
		{
			$sql.=" order by $orderby";	
		}
		if($limit!='')
		{
			$sql.=" limit $limit";
		}		
		
		$query = $this->db->query($sql);
		if($return=='')
		{
			return $query->row();
		}
		else
		{
			return $query->result();
		}
	}
	function getNextid($value,$primary_key,$table_name)
	{
		$sql="select $primary_key as max from $table_name where $primary_key>$value order by $primary_key desc limit 0,1";
		$query = $this->db->query($sql);
		$result=$query->row();	
		return $result->max;
	}
	function getPrevid($value,$primary_key,$table_name)
	{
		$sql="select $primary_key as min from $table_name where $primary_key<$value order by $primary_key desc limit 0,1";
		$query = $this->db->query($sql);
		$result=$query->row();	
		return $result->min;
	}
	function report_query($sql)
	{
		$query = $this->db->query($sql);
		return $query->result();

	}
		function getdetails_($query,$offset='0',$limit='1')
	{
		$sql="select ".$query." LIMIT $offset,$limit";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function getInfo_($query,$return='')
	{
		$query=$this->db->query($query);
		
		if($return=='')
		{
			return $query->row();
		}
		else
		{
			return $query->result();
		}

	}
	
	function getnumofdetail($query)
	{
		$query = $this->db->query($query);
		return $query->num_rows(); 
	}

	
}
?>