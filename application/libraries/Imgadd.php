<?php

	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Imgadd
	{
			
		function ImgSet($file,$path,$valid='')
		{
		
			$name	=	$file['name'];
			$type	=	$file['type'];
			 $tmp_name	=	$file['tmp_name'];
			$size	=	$file['size'];
			$filename=time().$name;
			 
			if($valid=='image' || $valid=='')
			{
				
				if($type=='image/jpeg' || $type=='image/jpg' || $type=='image/gif' || $type=='image/png' || $type=='image/bmp')
				{
					
					
					/*$filename = compress_image($_FILES["file"]["tmp_name"], $url, 80);
					if($filename)
					{
						echo "yes";
						return true;	
					}
					else
					{
						return false;	
					}*/
						$source_url= $tmp_name;
						$info = getimagesize($source_url);
						if ($info['mime'] == 'image/jpeg'){
								$image = imagecreatefromjpeg($source_url);
						}
						elseif ($info['mime'] == 'image/gif'){
								$image = imagecreatefromgif($source_url);
						}
						elseif ($info['mime'] == 'image/png'){
								$image = imagecreatefrompng($source_url);
						}
						else
						{
							$array	=	array('flag'=>'error','msg'=>'Extention error !File not Upload');
						}
					$imgup=	imagejpeg($image,$path.$filename, 60);
					
					if($imgup ==1)
					{
						$array	=	array('flag'=>'success','msg'=>$filename);
						return $array;
					}
					else
					{
						$array	=	array('flag'=>'error','msg'=>'File not Upload');
						
					}
					 
				}
				else
				{
					$array	=	array('flag'=>'error','msg'=>'Please Upload Valid File');
				}
				return $array;
			}
			else if($valid=='doc')
			{
				//$file_ext	=	'';
			//$file_ext=strtolower(end(explode('.',$name)));          
				if($type=='application/pdf' || $type=='application/docx' || $type=='application/doc' )
				{
					if(move_uploaded_file($tmp_name,$path.$filename))
					{
						$array	=	array('flag'=>'success','msg'=>$filename);
						return $array;
					}
					else
					{
						$array	=	array('flag'=>'errorup','msg'=>'File not Upload');
					}
				}
				else
				{
					$array	=	array('flag'=>'errorext','msg'=>'Please Upload Valid File');
				}
				return $array;
			}
		}
		
}
?>