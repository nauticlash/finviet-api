<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Redis_client
{
    /**
     * CodeIgniter instance.
     * refer to: https://github.com/phpredis/phpredis#installation
     * @var object
     */
    private $redis;
    private $locationPrefixKey = "bungkusit_pos_";
    private $orderPrefixKey = "bungkusit_order_";
    private $orderChangedPrefixKey = "bungkusit_order_changed_";
    private $locationExpiredTime = 10;
    private $orderExpiredTime = 300;
    private $orderChangedExpiredTime = 300; //5 minutes
    private $newOrderChannel = 'new_order_notification';

    public function __construct()
    {
        //establish Redis connection
        $host = config_item('redis_host');
        $port = config_item('redis_port');
        $timeout = config_item('redis_timeout');
        $this->redis = new Redis();
        $this->redis->pconnect($host, $port, $timeout);
        if (!empty(config_item("redis_location_prefix"))) {
            $this->locationPrefixKey = config_item("redis_location_prefix");
        }

        if (!empty(config_item("redis_location_expired_time"))) {
            $this->locationExpiredTime = config_item("redis_location_expired_time");
        }
        if (!empty(config_item("redis_changed_prefix_key"))) {
            $this->orderChangedPrefixKey = config_item("redis_changed_prefix_key");
        }
        if (!empty(config_item("redis_changed_expired_time"))) {
            $this->orderChangedExpiredTime = config_item("redis_changed_expired_time");
        }
        if (!empty(config_item("redis_order_prefix_key"))) {
            $this->orderPrefixKey = config_item("redis_order_prefix_key");
        }
        if (!empty(config_item("redis_order_expired_time"))) {
            $this->orderExpiredTime = config_item("redis_order_expired_time");
        }

    }

    /**
     * Add position of a user to redis
     */
    public function updateLocation($userid, $latitude, $longitude)
    {
        log_message('info', 'set location for key:' . $this->locationPrefixKey . $userid . ', expired:' . $this->locationExpiredTime . ', value:' . $latitude . ',' . $longitude . ',' . date('Y-m-d H:i:s', time()) . ',' . $userid);
        // return $this->redis->setEx($this->locationPrefixKey . $userid, $this->locationExpiredTime, $latitude . ',' . $longitude . ','.date('Y-m-d H:i:s', time()).','.$userid);
        return $this->redis->set($this->locationPrefixKey . $userid, $latitude . ',' . $longitude . ",'" . date('Y-m-d H:i:s', time()) . "'," . $userid, $this->locationExpiredTime);

    }
    public function getLocation($userid)
    {
        $position = $this->redis->get($this->locationPrefixKey . $userid); //format: lat,lng,Y-m-d H:i:s,userid
        if (!empty($position)) {
            return explode(',', $position);
        }
        return false;
    }

    /**
     * get all current cached locations
     */
    public function getCachedLocationValues()
    {
        $keys = $this->redis->keys($this->locationPrefixKey . '*');
        $values = $this->redis->mGet($keys);
        return $values;
    }
    /**
     * Clear all location data on Redis
     */
    public function clearAllLocations()
    {
        $keys = $this->redis->keys($this->locationPrefixKey . '*');
        return $this->redis->delete($keys);
    }
    /**
     * When rider switches to offline, remove the key in geodata
     */
    public function removeUserTracking($userid)
    {
        return $this->redis->delete($this->locationPrefixKey . $userid);
    }

    public function setChangedOrderDetail($orderId, $changed = true)
    {
        log_message('info', 'order changed:' . $orderId);
        return $this->redis->set($this->orderChangedPrefixKey . '_' . $orderId, '' . $changed, $this->orderChangedExpiredTime);
    }

    public function getChangedStatus($orderId)
    {
        return $this->redis->get($this->orderChangedPrefixKey . '_' . $orderId);
    }

    /**
     * cache order details by type (user or driver)
     */
    public function setOrderDetails($orderDetail, $type = 'user', $expired = 300)
    {
        $orderId = $orderDetail->orderid;

        log_message('info', 'cache order:' . $orderId);
        return $this->redis->set($this->orderPrefixKey . $type . '_' . $orderId, json_encode($orderDetail), $this->orderExpiredTime);
    }

    /**
     *
     */
    public function getOrderDetails($orderId, $type)
    {
        log_message('info', 'cache getOrderDetails:' . $orderId);
        $orderDetail = $this->redis->get($this->orderPrefixKey . $type . '_' . $orderId);

        if (!empty($orderDetail)) {
            log_message('info',  $orderDetail);
            $orderDetail = json_decode( $orderDetail);
            log_message('info',  'RIDER:'. $orderDetail->rider_id);
            if (!empty($orderDetail) && !empty($orderDetail->rider_id)) {
                $rider_id = $orderDetail->rider_id;
                $cachedLocation = $this->getLocation($rider_id); //[lat,lng,date,riderid]
                // log_message('info', 'cache rider location:' . $cachedLocation);
                if (!empty($cachedLocation)) {
                    $orderDetail->lat = $cachedLocation[0];
                    $orderDetail->lng = $cachedLocation[1];
                }
            }
        }
        return $orderDetail;
    }

    public function deleteOrderDetails($orderId, $type)
    {
        return $this->redis->delete($this->orderPrefixKey . $type . '_' . $orderId);
    }

    public function sendNewOrderNotification($orderData)
    {
        log_message('info', 'publish for key:' . $this->newOrderChannel . ', value:' . json_encode($orderData));
        return $this->redis->publish($this->newOrderChannel, json_encode($orderData));
    }
}
