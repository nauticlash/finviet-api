<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Redis_client
{
    /**
     * CodeIgniter instance.
     * refer to: https://github.com/phpredis/phpredis#installation
     * @var object
     */
    private $redis;
    private $geoKey = "bungkusit_pos";

    public function __construct()
    {
        //establish Redis connection
        $host = config_item('redis_host');
        $port = config_item('redis_port');
        $timeout = config_item('redis_timeout');
        $this->redis = new Redis();
        $this->redis->pconnect($host, $port, $timeout);
        if (!empty(config_item("redis_geo_key"))) {
            $this->geoKey = config_item("redis_geo_key");
        }

    }

    /**
     * Add position of a user to redis
     */
    public function updateLocation($userid, $latitude, $longitude)
    {
        $i = $this->redis->geoAdd($this->geoKey, $longitude, $latitude, $userid);

        if ($i === false) {
            log_message("error", "Cannot add location to REDIS");
            return false;
        }
        log_message('info', "update location to redis successful");

        //todo: publish to update db
        return true;
    }

    /**
     * geoRadius with Distance asc, and Coordinates
     */
    public function getRadius($latitude, $longitude, $radius, $unit = 'mi')
    {
        $options = ['WITHDIST', 'ASC', 'WITHCOORD'];
        // $options['count'] = 20;
        return $this->redis->geoRadius($this->geoKey, $longitude, $latitude, $radius, $unit, $options);
    }

    /**
     * geoRadius with Distance asc, and Coordinates.
     * Result does not contain current userid
     */
    public function geoRadiusByMember($userid, $radius, $unit = 'mi')
    {
        $options = ['WITHDIST', 'ASC', 'WITHCOORD'];
        // $options['count'] = 20;
        return $this->redis->geoRadiusByMember($this->geoKey, $userid, $radius, $unit, $options);
    }

    /**
     * Clear all location data on Redis
     */
    public function clearLocation()
    {
        return $this->redis->delete($this->geoKey);
    }
    /**
     * When rider switches to offline, remove the key in geodata
     */
    public function removeUserTracking($userid)
    {
        return $this->redis->zDelete($this->geoKey, $userid);
    }
}
